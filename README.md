# Índice

- [Intro](Intro/Intro.md)
  
  - [Por que Kubernetes?](Intro/Intro.md#por-que-kubernetes)
  - [Kubernetes _Components_](Intro/Intro.md#kubernetes-components)
  - [Kubernetes Flavors](Intro/Intro.md#kubernetes-flavors)

<br>

- [Instalação/Instanciamento](Install/Install.md)
  
  - [Vanilla](Install/Vanilla.md#kubernetes-vanilla)
    - [Especificações Técnicas](Install/Vanilla.md#especificações-técnicas)
    - [Modulos do Kernel para o K8S](Install/Vanilla.md#modulos-do-kernel-para-o-k8s)
    - [De cgroups para o systemd](Install/Vanilla.md#de-cgroups-para-o-systemd)
    - [kubectl, kubeadm e kubelet](Install/Vanilla.md#kubectl-kubeadm-e-kubelet)
    - [Iniciando o cluster _kubernetes_](Install/Vanilla.md#iniciando-o-cluster-k8s)
    - [Configurando o weave-net como _pod-network controller_](Install/Vanilla.md#configurando-o-weave-net-como-pod-network-controller)
  
  - [Vanilla HA (High Available)](Install/VanillaHA.md#kubernetes-vanilla-ha-high-available)
    - [Especificações Técnicas - HA](Install/VanillaHA.md#especificações-técnicas-ha)
    - [Instalação do HAProxy](Install/VanillaHA.md#instalação-do-haproxy)
    - [Iniciando o cluster _kubernetes_ multimaster](Install/VanillaHA.md#iniciando-o-cluster-kubernetes-multimaster)
  
  - [K3s](Install/K3s.md)
    - [Especificações técnicas](Install/K3s.md#especificações-técnicas)
    - [Instalação](Install/K3s.md#instalação)
    - [Multi-node](Install/K3s.md#multi-node)
  
  - [MicroK8](Install/Microk8s.md)
    - [Especificações técnicas](Install/Microk8s.md#especificações-técnicas)
    - [Instalação](Install/Microk8s.md#instalação)
    - [Ajustes](Install/Microk8s.md#ajustes)
  
  - [GKE - _user interface_](Install/GKE-ui.md) 
    - [Especificações técnicas](Install/GKE-ui.md#especificações-técnicas)
    - [Instanciamento](Install/GKE-ui.md#instanciamento)
    - [Ajustes](Install/GKE-ui.md#ajustes)
    - [Works pela UI](Install/GKE-ui.md#works-pela-ui)
    - [Deletando o cluster](Install/GKE-ui.md#deletando-o-cluster)

  - [GKE - _gcloud package_](Install/GKE-gdk.md)
    - [Especificações técnicas](Install/GKE-gdk.md#especificações-técnicas)
    - [Instalação](Install/GKE-gdk.md#instalação)
    - [Instanciamento](Install/GKE-gdk.md#instanciamento)
    - [Works pelo console](Install/GKE-gdk#works-pelo-console)
    - [Deletando o cluster](Install/GKE-gdk.md#deletando-o-cluster)

- [Gerenciamento do Kubectl](Install/Kubectl.md)

<br>

- [Deploy das aplicações](Pods/Pods.md#deploy-das-aplicações)
  
  - [Os _Pods_ são containers?](Pods/Pods.md#os-pods-são-containers)
  
  - [A tríade _Pod_ + _Replicaset_ + _Deployment_](Pods/Pods.md#a-tríade-pod--replicaset--deployment)
    - [Criando o primeiro _Pod_](Pods/Pods.md#criando-o-primeiro-pod)
    - [Criando o primeiro _Replicaset_](Pods/Pods.md#criando-o-primeiro-replicaset)
    - [Criando o primeiro _Deployment_](Pods/Pods.md#criando-o-primeiro-deployment)
  
  - [Persistindo dados](Pods/Pods.md#persistindo-dados)
    - [Gerenciando dados temporários com EmptyDir](Pods/Pods.md#gerenciando-dados-temporários-com-emptydir)
    - [Persistência de dados com Persistent Volume (PV) e Persistent Volume Claim (PVC)](Pods/Pods.md#persistência-de-dados-com-persistent-volume-pv-e-persistent-volume-claim-pvc)
      
      - [NSF Server](Pods/Pods.md#nsf-server)
      - [GlusterFS](Pods/Pods.md#glusterfs)
      - [Criando os primeiros PV e PVC](Pods/Pods.md#criando-os-primeiros-pv-e-pvc)

    - [_StorageClass_](Pods/Pods.md#storageclass)
  
  - [Outros controladores de _Pod_](Pods/Pods.md#outros-controladores-de-pod)
    - [DaemonSet](Pods/Pods.md#daemonset)
    - [StatefulSet](Pods/Pods.md#statefulset)
    - [Jobs e CronJobs](Pods/Pods.md#jobs-e-cronjobs)

  
  - [Gerenciando os _Pods_](Pods/Pods.md#gerenciando-os-pods)
    - [Limitando recursos no _Pod_](Pods/Pods.md#limitando-recursos-no-pod)
    - [Comandos, Argumentos e Variáveis](Pods/Pods.md#comandos-argumentos-e-variáveis)
    - [_Init container_](Pods/Pods.md#init-containers)
    - [_Pods_ prioritários](Pods/Pods.md$pods-prioritários)

<br>

- [Aplicações como _services_](Expose/Expose.md)
  
  - [Diferença entre _ClusterIP_, _NodePort_ e _LoadBalancer_](Expose/Expose.md#diferença-entre-clusterip-nodeport-e-loadbalancer)
    - [ClusterIP](Expose/Expose.md#clusterip)
    - [NodePort](Expose/Expose.md#nodeport)
    - [LoadBalancer](Expose/Expose.md#loadbalancer)  
    - [LoadBlancer MetalLB](Expose/Expose.md#loadbalancer-metallb)
  
  - [Integração com serviços externos - _externalNames_](Expose/Expose.md#intregação-com-serviços-externos--externalnames)
  
  - [_Ingress_](Expose/Expose.md#ingress)    
    - [NGINX Ingress](Expose/Expose.md#nginx-ingress)
    - [Traefik Ingress](Expose/Expose.md#traefik-ingress)
    - [Um Ingress na prática](Expose/Expose.md#um-ingress-na-prática)

<br>

- [Controle de ambiente](Environment/Env.md)
  
  - [Namespaces](Environment/Env.md#namespaces)
    - [Alocando recursos para o namespace](Environment/Env.md#alocando-recursos-para-o-namespace)
    - [Gerenciando recursos no namespace](Environment/Env.md#gerenciando-recursos-no-namespace)
    - [Configurações do namespace](Environment/Env.md#configurações-do-namespace)
    - [Regras para autorizações](Environment/Env.md#regras-para-autorizações)
  
  - [Cluster](Environment/Env.md#cluster)
    - [Autenticando um novo usuário](Environment/Env.md#autenticando-um-novo-usuário)
    - [Autorizando o novo usuário](Environment/Env.md#autorizando-o-novo-usuário)
  
