# Aplicações como _services_

Nessa seção falaremos de disponibilizar a aplicação ao destinatário final, uma parte crítica se tratando de uma aplicação distribuida em múltiplos pontos de uma rede. Cada um desses pontos gera o que o Kubernetes chama de _Endpoints_, que nada mais é do que um IP próprio dentro no nível da rede a quem pertence. Interagir com todos esses endpoints para expor um service ao usuário pode ser uma tarefa complexa de se fazer, por sorte a API do Kubernetes abstrai isso basicamente em dois objetos: os _Services_ e o _Ingress_.

O ponto chave dos serviços no **k8s** é pensar que o tempo todo, novos IPs podem ser atribuidos aos _Pods_ e "alguém" precisa saber quais são esses _endpoints_ sempre que atualizam para que o serviço possa ser entregue. É justamente por isso que existem os _Services_ e o _Ingress_, eles serão os "guias" que irão exercer essa tarefa.

## Diferença entre _ClusterIP_, _NodePort_ e _LoadBalancer_

No **k8s** existem basicamente três tipos de _Services_:

1. ClusterIP: Apenas usuários dentro da própria rede do _cluster_ serão capazes de interagir com ele.
2. NodePort: Usuários externos ao _cluster_ poderam interagir.
3. LoadBalancer: Tem a função de balancear as requisões externas (pois eles recebem IPs externos) em múltiplos serviços.

Esses objetos seguem uma "hierarquia de deploy" (aqui o nome não é mais deploy porque a ideia de um objeto "Service" é uma abstração) parecida com o tríade Pod/Replicaset/Deployment, ou seja, um LoadBalancer cria um NodePort e um ClusterIP, um NodePort cria um ClusterIP e o ClusterIP pode ser exposto sozinho.

### ClusterIP

Vamos começar criando um _Deployment_ do `nginx`:

```json
{
    "kind": "Deployment",
    "apiVersion": "apps/v1",
    "metadata": {
        "name": "nginx"
    },
    "spec": {
        "replicas": 1,
        "selector": {
            "matchLabels": {
                "run": "nginx"
            }
        },
        "template": {
            "metadata": {
                "labels": {
                    "run": "nginx"
                }
            },
            "spec": {
                "containers": [
                        {
                        "name": "nginx",
                        "image": "nginx", 
                        "ports": [
                            {
                                "containerPort": 80
                            }
                        ]
                    }
                ],
                "restartPolicy": "Always",
                "dnsPolicy": "ClusterFirst"
            }    
        }
    }
}
```

Verifique se o serviço está rodando:

```
kubectl get deploy -w

----
NAME    READY   UP-TO-DATE   AVAILABLE   AGE
nginx   0/1     1            0           11s
nginx   1/1     1            1           34s

```

Crie um arquivo `clusterip.json` e insira o seguinte conteúdo (repare que ele segue a mesma estrutura básica de outros objetos **k8s**):

```json
{
    "kind": "Service",
    "apiVersion": "v1",
    "metadata": {
        "labels": {
            "run": "nginx"
        },
        "name": "nginx-svc"
    },
    "spec": {
        "ports": [
            {
                "port": 80,
                "targetPort": 80
            }
        ],
        "selector": {
            "run": "nginx"
        },
        "type": "ClusterIP"
    }
}
```

Confira se o _service_ foi criado:

```
kubectl get svc
---
NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1      <none>        443/TCP   30d
nginx-svc    ClusterIP   10.98.44.118   <none>        80/TCP    13s
```

Agora basta conectar (ssh) em algum dos nodes e fazer um `curl` para esse endereço de IP que o conteúdo da página do `nginx` aparecerá. É possível alterar o manifesto do ClusterIP para permitir a afinidade de conexão a um _Pod_, ou seja, ele reconhece o IP do usuário e o direciona sempre para o mesmo _Pod_, ficaria assim:

```json
{
    "kind": "Service",
    "apiVersion": "v1",
    "metadata": {
        "labels": {
            "run": "nginx"
        },
        "name": "nginx-svc"
    },
    "spec": {
        "ports": [
            {
                "port": 80,
                "targetPort": 80
            }
        ],
        "selector": {
            "run": "nginx"
        },
        "sessionAffinity": "ClientIP",
        "type": "ClusterIP"
    }
}
```
> Nos Statefulset essa estratégia é interessante pois os dados do usuário podem ocupar espaço em apenas um PV, já que as requests sempre cairão no mesmo Pod.

### NodePort
Delete o service anterior e vamos criar um novo manifesto do tipo NodePort

```json
{
    "kind": "Service",
    "apiVersion": "v1",
    "metadata": {
        "labels": {
            "run": "nginx"
        },
        "name": "nginx-svc"
    },
    "spec": {
        "ports": [
            {
                "port": 80,
                "targetPort": 80,
                "nodePort": 32000
            }
        ],
        "selector": {
            "run": "nginx"
        },
        "sessionAffinity": "ClientIP",
        "type": "NodePort"
    }
}
```
Os services do tipo NodePort são montados no range entre 30000 e 32767. Repare que na prática a única diferença é a inclusão da declaração "nodePort" nos valores que **k8s** reserva justamente para isso. 


### LoadBalancer

Imagine uma situação em que tenhamos 2 _Pod_ do nginx rodando no mesmo deployment, o serviço estaria expondo os dois na mesma porta 32000 o que geraria instabilidade. O LoadBalancer (LB) resolve essa questão e passa a rotear as requisões de acesso. 

Delete o serviço anterior e crie:

```json
{
    "kind": "Service",
    "apiVersion": "v1",
    "metadata": {
        "labels": {
            "run": "nginx"
        },
        "name": "nginx-svc"
    },
    "spec": {
        "ports": [
            {
                "port": 80,
                "targetPort": 80
            }
        ],
        "selector": {
            "run": "nginx"
        },
        "sessionAffinity": "ClientIP",
        "type": "LoadBalancer"
    }
}
```

Repare que o LoadBalancer recebe a solicitação na porta 80 e redireciona para o pod que estiver disponível segundo o esquema de round-robin. É possível que dependendo da instalação o EXTERNAL-IP fique <pending>, isso acontece porque normalmente quem fornece o serviço de LoadBalancer é a nuvem pública. Em _bare metal_ existe uma opção de ser contornar isso, instalando o [MetalLB](https://metallb.universe.tf/).

Além do serviço de LoadBalancer em de uma _cloud_ pública existe a possibilidade de existir um software com a mesma função no local aonde o cluster está instalado. Esse é um cenário simulado pelo **k8s** no DockerDesktop, só que com o LB apontando para o `localhost`. 

Nas _clouds_ públicas, ainda que o LB receba um IP externo e seja uma situação bem comum usá-lo como acesso direto para serviços, a boa prática recomenda que se crie um objeto do tipo _Ingress_ e não exponha os IPs externos do cluster.

### LoadBalancer MetalLB

> Antes de instalar o MetalLB confira se a instalação do seu **k8s** já não possui um LB como serviço, como mencionado acima.

Instale o MetalLB:

```
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.3/manifests/namespace.yaml
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.3/manifests/metallb.yaml
```

Encripte a comunicação entre os _Pods_

```
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
```

Agora será necessário especificar o range de IPs que serão alocados para o MetalLB utilizar criando um ConfigMap:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: address-pool-1
      protocol: layer2
      addresses:
      - 192.168.2.128-192.168.2.254
```

A partir de agora quando um LoadBalancer for criado algum desses IPs externos serão alocados para o service. 

## Integração com serviços externos - _externalNames_
Em cenários onde o Kubernetes irá ser implementado em locais com serviços já existentes ou quando o _cluster_ precisa interagir com serviços externos, o externalNames é objeto ideal. 

Imagine que a nossa aplicação precise fazer chamadas constantes para uma api no endereço `api.externa.com`. Com o externalNames é possível criar um serviço dentro do cluster apontando para esse endereço, de modo que a chamada da nossa aplicação se mantenha sempre a mesma, apontada para esse service. Isso evita alterações de código por mudanças de endereços DNS que não são sua resposibilidade, bastaria alterar apenas o services do tipo externalNames. 

A seguir, temos um examplo do objeto external names:

```json
{
  "kind": "Service",
  "apiVersion": "v1",
  "metadata": {
    "name": "api-externa"
  },
  "spec": {
    "ports": [
      {
        "port": 3000,
        "protocol": "TCP",
        "targetPort": 443
      }
    ],
    "type": "ExternalName",
    "externalName": "api.externa.com"
  }
}
```

> Repare que ainda é possível configurar esses acessos externos com variáveis de ambiente, no caso com o configMap.

<br>

## _Ingress_
A ideia de implementar um Ingress não deve seguir o pensamento do que foi exposto nos _Services_, na realidade o _Ingress_ deve ser visto como um conjunto de regras que permitirão que o tráfego externo chegue ao _cluster_, geralmente em https, ou seja com certificado TLS e verificação, sem essas regras esse tráfego seria descartado.

**Então basicamente a ideia principal é, quando um request chegar na porta 80 (padrão) do host o Ingress deve detecetar e redirecionar para o local determinado.**

Para que isso ocorra é necessário ter um Ingress controller, o agente responsável por essa função. 

Caso o ambiente seja _cloud_ os Ingress controller serão os API _gateways_ dos _providers_.
Caso o ambiente seja alguma distro como o K3s, o Ingress controller já vem integrado, normalmente é o Traefik. Consulte se o traefik está no namespace kube-system com:

```
kubectl get po --all-namespaces
```

Caso contrário, será necessário instalar. Manualmente, instalar um ingress pode ser trabalhoso, mas existem opções como o Helm ou repositórios com todos os yaml que facilitam isso.

<br>

#### NGINX Ingress
Para instalar o NGINX Ingress aplique o seguinte file:

```sh
kubectl apply -f 
https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.34.1/deploy/static/provider/baremetal/deploy.yaml
```

<br>

> Por curiosidade, dentro dele existem diversos componentes necessários para a implementação de um microserviço no Kubernetes, como: namespace, service account, cluster role, jobs e outros. Vale a pena dar uma conferida em como essa dinâmica funciona. 

A seguir confira em qual porta o _Ingress controller_ está redirecionando

```
kubectl get svc --all-namespaces
```
Acesse o localhost nessa porta e verá uma mensagem de not found, o que indica que o _Ingress controller_ já está trabalhando. 

```
curl localhost

---
404 page not found
```

<br>

#### Traefik Ingress
Antes de tudo, vale a pena dizer que falta documentação aberta para usar o Traefik como _Ingress Controller_ acima da versão 2.0.

Nesse tópico estaremos seguindo as instruções disponíveis [aqui](https://blog.tomarrell.com/post/traefik_v2_on_kubernetes).

O `kubectl` possui um recurso chamado `kustomize` que permite reunir recursos do k8s em um único file, desde que todos estejam no mesmo diretório, para crie esse diretório, se mova para lá e crie um arquivo chamado `kustomization.yaml`.

```
mkdir traefik-set-up
cd traefik-set-up
vim kustomization.yaml
```

Copie e cole o seguinte conteúdo:

```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
  - cluster-role.yaml
  - cluster-role-binding.yaml
  - service-account.yaml
  - daemon-set.yaml

namespace: kube-system

commonLabels:
  app: traefik
```

Salve e feche o arquivo. Todos os elementos que precisam ser criados estão na declaração `resources` do arquivo que acabou de ser criado. Sendo assim, na mesma pasta, crie um arquivo para a _service account_ com o nome exposto acima e insira o seguinte conteúdo:

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: traefik-ingress-controller
```

Agora crie uma _Cluster Role_ e um _Cluster Role Binding_ (veremos suas funções nos próximos capítulos) com os mesmos nomes já expostos acima e com os seguintes conteúdos:

```yaml
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: traefik-ingress-controller
rules:
  - apiGroups:
      - ""
    resources:
      - services
      - endpoints
      - secrets
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - extensions
    resources:
      - ingresses
    verbs:
      - get
      - list
      - watch
  - apiGroups:
    - extensions
    resources:
    - ingresses/status
    verbs:
    - update
```

<br>

```yaml
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: traefik-ingress-controller
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: traefik-ingress-controller
subjects:
- kind: ServiceAccount
  name: traefik-ingress-controller
```

Por fim crie o DaemonSet responsável pelo _Ingress controller_ de fato:

```yaml
kind: DaemonSet
apiVersion: apps/v1
metadata:
  name: traefik-daemon-set
spec:
  template:
    spec:
      serviceAccountName: traefik-ingress-controller
      terminationGracePeriodSeconds: 60
      containers:
      - image: traefik:v2.2
        name: traefik-ingress-lb
        ports:
        - name: http
          containerPort: 80
          hostPort: 80
        - name: https
          containerPort: 443
          hostPort: 443
        - name: admin
          containerPort: 8080
          hostPort: 8080
        securityContext:
          capabilities:
            drop:
            - ALL
            add:
            - NET_BIND_SERVICE
        args:
        # Enable the dashboard without requiring a password. Not recommended
        # for production.
        - --api.insecure
        - --api.dashboard

        # Specify that we want to use Traefik as an Ingress Controller.
        - --providers.kubernetesingress

        # Define two entrypoint ports, and setup a redirect from HTTP to HTTPS.
        - --entryPoints.web.address=:80
        - --entryPoints.websecure.address=:443
        - --entrypoints.web.http.redirections.entryPoint.to=websecure
        - --entrypoints.web.http.redirections.entryPoint.scheme=https

        # Enable debug logging. Useful to work out why something might not be
        # working. Fetch logs of the pod.
        # - --log.level=debug

        # Let's Encrypt Configurtion.
        - --certificatesresolvers.default.acme.email=dr2pedro@codeplaydata.com
        - --certificatesresolvers.default.acme.storage=acme.json
        - --certificatesresolvers.default.acme.tlschallenge
        # Use the staging ACME server. Uncomment this while testing to prevent
        # hitting rate limits in production.
        - --certificatesresolvers.default.acme.caserver=https://acme-staging-v02.api.letsencrypt.org/dire
```

<br>

Repare na lista de argumentos:
 - `api insecure` e `api dashboard` são opções passadas ao Traefik para permitir o acesso ao dashboard sem usuário e senha, **não utilize essas opções em ambiente de produção**;
- `provider kubernetes ingress` é **obrigatório** e diz ao Traefik que ele deve agir como _Ingress controller_;
- as opções `entrypoints` configuram as portas de entrada default para `web` e para `websecure`, faz o redirect de uma para a outra e define o protocolo https como default.
- os certificates resolvers configuram o let's encrypt, o último argumento está redirecionando para o servidor de teste do let's encrypt, caso esteja em produção comente essa linha.

Agora que todos os arquivos foram criados, basta criar todos os recursos com:

```sh
kubectl apply -k .
```

Pronto agora caso acesse o localhost ele irá amostrar o `404 
Not Found` e caso acesse na porta 8080 o dashboard aparecerá.

<br>

### Um Ingress na prática
Com o seu _Ingress controller_ pronto, o passo seguinte é criar o deploy de uma aplicação, que nesse caso será do `nginx`:

```json
{
    "kind": "Deployment",
    "apiVersion": "apps/v1",
    "metadata": {
        "name": "nginx"
    },
    "spec": {
        "replicas": 1,
        "selector": {
            "matchLabels": {
                "run": "nginx"
            }
        },
        "template": {
            "metadata": {
                "labels": {
                    "run": "nginx"
                }
            },
            "spec": {
                "containers": [
                        {
                        "name": "nginx",
                        "image": "nginx", 
                        "ports": [
                            {
                                "containerPort": 80
                            }
                        ]
                    }
                ],
                "restartPolicy": "Always",
                "dnsPolicy": "ClusterFirst"
            }    
        }
    }
}
```

Em seguida exponha esse serviço em um ClusterIP, ou seja, teoricamente apenas usuários de dentro do _cluster_ teriam acesso a aplicação.

```json
{
    "kind": "Service",
    "apiVersion": "v1",
    "metadata": {
        "labels": {
            "run": "nginx"
        },
        "name": "nginx-svc"
    },
    "spec": {
        "ports": [
            {
                "port": 80,
                "targetPort": 80
            }
        ],
        "selector": {
            "run": "nginx"
        },
        "sessionAffinity": "ClientIP"
    }
}
```

A seguir crie um objeto do tipo _Ingress_, que nesse caso será bem simples. 
- Dentro do `spec` esse objeto espera um array de `regras` e dentro do `metadata` o _Ingress controller_ espera que existam annotations. 
- Todas as opções de annotations estão disponíveis no site do [nginx](https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/) e do [traefik](https://doc.traefik.io/traefik/v1.7/configuration/backends/kubernetes/) ([aqui tem um blog](https://docs.giantswarm.io/advanced/ingress/configuration/) que auxilia demais no nginx).
- Cada regra é composta, opcionalmente, por: uma chave **host**, que indica pra quem o _Ingress_ vai setar a regra; e uma chave **http**.
- Cada chave **http** é composta por uma rota **path**, um tipo **pathType** e o que está por trás dessa rota **backend**.
- Cada **backend** é composto por um **service** de nome igual ao que foi criado e com a porta exata que o service está exposto. 

 sendo assim e estrutura básica ficaria assim:

```json
{
    "apiVersion": "networking.k8s.io/v1",
    "kind": "Ingress",
    "metadata": {
      "name": "minimal-ingress",
      "annotations": {
        "nginx.ingress.kubernetes.io/rewrite-target": "/"
      }
    },
    "spec": {
      "rules": [
        {
          "http": {
            "paths": [
              {
                "path": "/",
                "pathType": "Prefix",
                "backend": {
                  "service": {
                    "name": "nginx",
                    "port": {
                      "number": 80
                    }
                  }
                }
              }
            ]
          }
        }
      ]
    }
  }

```

Nesse nosso cenário a anotação colocada reescreve o caminho da URL, o que é necessário em casos de rotas diferentes entre o _Ingress_ e as rotas já definidas no service., o host é ocultado pois ele receberá requisições de qualquer lugar e o path é / que seria o mesmo diponível no `nginx` service na porta 80.

Implemente o _Ingress_ com:

```
kubectl apply -f <nome do file do ingress>.json
```

Acesse o localhost e verá uma mensagem do NGINX no navegador (mesmo com o service estando como ClusterIP!).
