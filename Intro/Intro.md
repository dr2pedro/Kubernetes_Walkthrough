## Introdução

Esse repositório é um guia da experiência prática do uso do Kubernetes (k8s) para o desenvolvimento de aplicações distribuídas. Seu conteúdo foi desenvolvido baseado na documentação oficial, alguns cursos livres (como o curso disponível pela [LinuxTips](https://school.linuxtips.io/) que eu super recomendo que façam!), artigos sobre o tema e outras fontes disponíveis online. 

**O ponto de vista adotado é do desenvolvedor, não do _sysadmin_, por isso a opção de construção dos manifestos, sempre que possível, se dará por JSON.**

Com a crescente demanda do mercado para profissionais de tecnologia com conhecimentos diferenciais dedicar-se à esse conteúdo pode ser uma oportunidade para se destacar na base de gerenciamento de containers e aplicações distribuídas. 

É inegável que nos últimos anos o Kubernetes assumiu uma posição de "Sistema Operacional" das _clouds_ públicas, podendo ser configurado para diferentes fins como _serveless_, quando usado no Knative; virtualização de _Virtual Machines_ como usado no Kubevirt; ou como _Data Science flow manager_, quando usado no Kubeflow.

Vale lembrar que uma das certificações oferecidas pela Cloud Native Computing Foundation é para desenvolvedores, a [CKAD](https://www.cncf.io/certification/ckad/), o que destaca ainda mais o papel multi-nicho que o Kubernetes desempenha.

<br>

### Por que Kubernetes?

Atualmente o Kubernetes é o melhor orquestrador de containers existente.
Existem alguns concorrentes que merecem destaque como [Nomad](https://www.nomadproject.io/) da Hashicorp mas a menor curva de aprendizado e o suporte da comunidade ainda são os destaques que levam o Kubernetes ao destaque.

<br>

**Ok, mas afinal, por que Kubernetes?**

<br>

É possível que em algum momento no _lifecicle_ de uma aplicação a quantidade de containers necessários para o desenvolvimento e produção suba consideravelmente.

A depender da complexidade desse cenário, será necessário o uso de um gerenciador de containers para que o desenvolvedor possa lidar com as configurações de cada um desses containers, viabilidade da comunicação entre eles (_network_), persistência dos dados, disponibilidade dos recursos de _hardware_, escalabilidade, instaciamento de novos _services_, _service discovery_, alta-disponibilidade, etc...

<br>

>**É para suprir essas necessidades que o Kubernetes existe.**

<br>

É possível que a resposta se o Kubernetes deve ou não ser usado seja algo pessoal do desenvolvedor. Atualmente existem abstrações que facilitam o seu uso.

<br>



### Kubernetes _Components_

O Kubernetes sem uma distro, chamado pela comunidade de _Vanilla_, é apresentado na seguinte estrutura:

![Kubernetes-Components](https://d33wubrfki0l68.cloudfront.net/2475489eaf20163ec0f54ddc1d92aa8d4c87c96b/e7c81/images/docs/components-of-kubernetes.svg)


- **Control Plane** No _cluster_ Kubernetes, esse é o node `main` que contém:

  - o **etcd**: base de dados de armazenamento de chave-valor
  - o **kube-scheduler**: agendador de containers, responsável por identificar uma nova solicitação de criação e alocar esse container em algum `node`
  - o **kube-controller-manager**: responsável por perceber e agir quando algum `node` cai, reponsável por manter o correto número de pods e gerencia as contas e tokens dos namespaces
  - o **cloud-controller-manager**: desempenha o mesmo papel do _controller_ anterior, mas é preparado para interagir com API da maioria das _clouds_ públicas

- **Node** No _cluster_ Kubernetes esses são os **"que fazem o trabalho pesado"** e contém:
  
  - **kubelet**: é o _agent_ responsável por implementar as especificações do `pod` que é passada pelo `controller`
  - **kube-proxy**: implementa as especificações de rede nos `nodes`
  
<br>

> Em _clouds_ públicas o control Plane é "abstraido" e não é cobrado. 

<br>

### Kubernetes Flavors

Assim como os OS (sistemas operacionais) diferentes, como Linux Ubuntu, CentOS ou Red Hat, os componentes do Kubernetes podem ser implementadas de formas diferentes, como a troca do etcd para o sqlite para banco de dados, e consequentemente gerarão _softwares_ diferentes.

Alguns deles estão disponibilizados como _PaaS_ (_Plataform as a Service_) por meio de _Clouds_ públicas, como:
- [Google Kubernetes Engine (GKE)](https://cloud.google.com/kubernetes-engine) 
- [Azure Kubernetes Service (AKS)](https://azure.microsoft.com/en-us/services/kubernetes-service/) 
- [Amazon Elastic Kubernetes Service (Amazon EKS)](https://aws.amazon.com/pt/eks). 

Outros são implementados como ambientes de desenvolvimento local, portanto **não devem ser usados em produção**. São eles: 
- [minikube](https://minikube.sigs.k8s.io/docs/)
- [Kubernetes in Docker - kind](https://kind.sigs.k8s.io/)
- [Docker Desktop - Kubernetes](https://docs.docker.com/docker-for-windows/kubernetes/).

Existem os "Kubernetes _light_", planejados para hardwares que trabalham com baixa demanda e/ou consumo de energia:
- [MicroK8s](https://microk8s.io/) 
- [k3s](https://k3s.io/). 

Ainda nesse ramo estão surgindo alguns que incluem dados de sensores IOT como componentes do sistema que é o caso do [Kubedge](https://kubeedge.io/en/).  

Finalmente os Kubernetes que são implemantados em _bare metal_, servidores próprios, como soluções empresariais. Desses podemos destacar três: 
- [Red Hat OpenShift Container Plataform](https://www.openshift.com/products/container-platform) 
- [Rancher](https://rancher.com/)
- [Nomad](https://www.nomadproject.io/)

<br>
