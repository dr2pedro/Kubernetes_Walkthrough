## GKE - gcloud CLI

A forma mais comum de gerenciar os cluster em clouds públicas é por meio de _softwares_ de _infra-as-code_ como o Terraform ou o Ansible. Entretanto, como alternativa (bem limitada) existem as CLIs/SDKs próprias de cada cloud.

### Especificações Técnicas
Uma das especificações técnicas também é uma limitação. Além da conta da GCP e um projeto criado é necessário ter um browser, ou seja, não da para usar apenas o console. Isso se deve ao fato de que a autenticação é feita por OAuth. 

### Instalação
Inicialmente será necessário ter o kubectl instalado

```
snap install kubectl --classic
```
A seguir, a SDK do Google Cloud Plataform (GCP):

```
snap install google-cloud-sdk --classic
```

Ao digitar apenas `gcloud` e ir passando de linha com o `enter` é possível saber todos os recursos disponíveis na plataforma que podem ser controlados pela SDK, para sair dessa lista digite Ctrl/Cmd+C

O primeiro passo para o instancimento na GCP é o login

```
gcloud auth login

---
Go to the following link in your browser:

    https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=325111111.apps.googleusercontent.com&redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&scope=openid+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcloud-platform+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fappengine.admin+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcompute+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Faccounts.reauth&state=z7JJHm4mdM6wc123456789QQPD647Rg&code_challenge=2xHzpXcYEMWKAIDFN5282zXwZFmZ5IC2p470&code_challenge_method=S256&prompt=consent&access_type=offline

Enter verification code:

```

Abra o browser no link especificado, faça o longin e copie o código dado no console.
Após a autenticação feita inicie o seu ambiente de trabalho com:

```
gcloud init

--- 

Welcome! This command will take you through the configuration of gcloud.

Settings from your current configuration [default] are:
core:
  account: <seu email>
  disable_usage_reporting: 'False'
  project: intro-ao-gke

Pick configuration to use:
 [1] Re-initialize this configuration [default] with new settings
 [2] Create a new configuration

Please enter your numeric choice:
```

Escolha a primeira opção

```
Your current configuration has been set to: [default]

You can skip diagnostics next time by using the following flag:
  gcloud init --skip-diagnostics

Network diagnostic detects and fixes local network connection issues.
Checking network connection...done.
Reachability Check passed.
Network diagnostic passed (1/1 checks passed).

Choose the account you would like to use to perform operations for
this configuration:
 [1] <seu email>
 [2] Log in with a new account
Please enter your numeric choice:

```

Confirme o email que deseja usar.

```
You are logged in as: [pedoidin@gmail.com].

Pick cloud project to use: 
 [1] intro-ao-gke
 [2] intro-gke
 [3] Create a new project
Please enter numeric choice or text value (must exactly match list
item):
```
Escolha quais do projeto deseja trabalhar. Ao final ele cria um arquivo oculta com as configurações escolhidas e já será possível instanciar o cluster.


### Instanciamento

Inicie o ciclo verificando quantos clusters já existem nesse projeto (toda a estrutura da GCP é orientada a projeto). 

```sh
gcloud container clusters list
```

Caso ainda não tenha criado experimente criar o primeiro com:

```
gcloud container clusters create --region us-central1-c cluster-1

---
WARNING: Starting in January 2021, clusters will use the Regular release channel by default when `--cluster-version`, `--release-channel`, `--no-enable-autoupgrade`, and `--no-enable-autorepair` flags are not specified.
WARNING: Currently VPC-native is not the default mode during cluster creation. In the future, this will become the default mode and can be disabled using `--no-enable-ip-alias` flag. Use `--[no-]enable-ip-alias` flag to suppress this warning.
WARNING: Starting with version 1.18, clusters will have shielded GKE nodes by default.
WARNING: Your Pod address range (`--cluster-ipv4-cidr`) can accommodate at most 1008 node(s).
WARNING: Starting with version 1.19, newly created clusters and node-pools will have COS_CONTAINERD as the default node image when no image type is specified.
Creating cluster cluster-1 in us-central1-c... Cluster is being health-checked (master is healthy)...done.
Created [https://container.googleapis.com/v1/projects/intro-ao-gke/zones/us-central1-c/clusters/cluster-1].
To inspect the contents of your cluster, go to: https://console.cloud.google.com/kubernetes/workload_/gcloud/us-central1-c/cluster-1?project=intro-ao-gke
kubeconfig entry generated for cluster-1.
NAME       LOCATION       MASTER_VERSION    MASTER_IP     MACHINE_TYPE  NODE_VERSION      NUM_NODES  STATUS
cluster-1  us-central1-c  1.18.16-gke.2100  34.71.86.106  e2-medium     1.18.16-gke.2100  3          RUNNING
```

### Parabéns, seu primeiro cluster está criado!

### Works pelo console
Automaticamente o gcloud cria o contexto necessário para o controle do cluster pelo kubectl que é possível de ser verificado com:

```
kubectl config get-contexts
```
Caso o contexto `*` apareça, experimente verificar os nodes do seu cluster com:

```
kubectl get nodes
```

### Deletando o cluster
Agora que o cluster já está criado, quando ele não for mais necessário, será preciso deletar, uma vez que nas clouds o pagamento é por tempo de uso, para isso, basta:

```
gcloud container clusters delete --zone us-central1-c cluster-1

--- 

The following clusters will be deleted.
 - [cluster-1] in [us-central1-c]

Do you want to continue (Y/n)?  y

Deleting cluster cluster-1...⠧
```

### Parabéns, cluster deletado. Sem mais cobranças.