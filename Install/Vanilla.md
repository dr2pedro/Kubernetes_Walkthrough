## Kubernetes _Vanilla_

Quando o Kubernetes é instalado em seu esquema original, usando a tríade do `kubeadm`, `kubelet` e `kubectl`, em sua configuração _default_, alguns desenvolvedores a chamam de _Vanilla_, fazendo referência ao Javascript sem _frameworks_ que possui o mesmo nome.

<br>

> Lembre-se que todo o processo deve ser feito em cada `node` até a parte do **Iniciando o cluster _kubernetes_** que possui a dinâmica diferente explicada no próprio tópico. 

<br>

### Especificações Técnicas
- 1 Máquina ou VM com 4GB de RAM, 20GB de HD e 2 cpus (para o node _main_)
- 2 Máquinas ou VM com 2GB de RAM, 20GB de HD e 2 cpus (para os _workers_)
- SO Linux, nesse tutorial foi utilizado Ubuntu focal
- um `Container Runtime` instalado em todas elas. Dependendo da versão do Kubernetes ainda é possível utilizar o `Docker`, as versões mais atuais recomendam o uso de outro `Runtime` como o [Containerd](https://containerd.io/) ou o [Podman](https://podman.io/).

<br>

### Modulos do Kernel para o K8S
Alguns módulos do Kernel do Linux devem estar disponíveis para o funcionamento correto do **k8s**. Para isso, crie um arquivo de configurações dentro da pasta `/etc/modules-load.d/` chamado `k8s.conf`:

```sh
touch /etc/modules-load.d/k8s.conf
vim /etc/modules-load.d/k8s.conf
```

E insira esse conteúdo:

```vim
br_netfilter
ip_vs
ip_vs_rr
ip_vs_sh
ip_vs_wrr
nf_conntrack_ipv4
```

<br>

### De cgroups para o systemd

Por _default_ como gerenciador de recursos da VM/Hardware alguns _Containers Runtime_ utilizam o `cgroups`, porém, o **k8s** faz o uso do próprio `systemd` para essa função, o Docker é um desses casos. Caso esteja utilizando o Docker, é possível trocar essa config no criando um arquivo `.json` dentro da pasta `etc/docker/` chamado `daemon.json` com o seguinte conteúdo (para outros Runtimes como Podman ou Containerd consulte a própria documentação):

```json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
```

<br>

> Usar o Vim para editar .json pode ser trabalhoso, um modo mais simples seria usar o comando `cat`: 

```
cat<<EOF> "arquivo" 
'conteúdo' 
EOF
```

<br>

Em seguida, crie uma pasta que irá armazenar as informações do docker dentro do diretório do `systemd` assim:

```sh
mkdir -p /etc/systemd/system/docker.service.d
```

Por fim, reinicie o Docker:

```shell
systemctl daemon-reload
systemctl restart docker
```

Caso tudo tenha dado certo, ao digitar:
```
docker info | grep -i cgroup
``` 
em seu terminal deve aparecer:

![terminal-1](./assets/terminal-1.png)

<br>

>Repare que ele está avisando que não tem suporte à memória swap, esse é outro pré-requisito. Ela pode ser removida assim:

<br>

```shell
swapoff -a #caso a máquina/VM reinicie esse comando deve ser repetido
```

<br>

### kubectl, kubeadm e kubelet

A partir de agora estaremos de fato instalando o K8S. Para isso basta adicionar o repositório do kubernetes na lista usando a chave solicitada ao google cloud e instalar os seguintes componentes:

- **`kubectl`** - a linha de comando (CLI) do Kubernetes;
- **`kubeadm`** - o responsável por tomar as ações necessárias para a viabilidade de um cluster;
- **`kubelet`** - quem interpreta as especificações de um _Pod_ (menor unidade do **k8s**) no `node`.

<br>

```sh
apt update
apt install -yapt-transport-https gnupg2
curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg -o apt-key.gpg  #salvando a chave
apt-key add apt-key.gpg  #adicionando a chave ao repo
apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"  #adicionando repositório. Esse comando vem da doc deles, é bom conferir se atualizou.
apt update
apt install -y kubelet kubeadm kubectl
```

<br>

### Iniciando o cluster K8S

O _cluster_ pode ser inciado no _node_ `main` com um simples comando: 

```sh
kubeadm init

# caso precise montar em algum IP que não seja o da sua rede default especifique ele na entrada assim:

kubeadm init --apiserver-advertise-address 192.168.01.2
```


Esse início pode demorar um pouco caso as imagens para os _Pods_ da API do Kubernetes não existam salvas na VM. É possível adiantar esse `pull` antes de iniciar o cluster com: 

```sh
kubeadm config images pull
```
Na própria saída ao final da incialização do _cluster_ será sugerido registrar o "contexto" do k8s para o usuário que criou esse cluster (essa parte ficará mais clara a seguir) e incluir `nodes` _workers_ com o `kubeadm join --token`. Repare que nessa sugestão de comando existe um `token`, um IP do _main_ `node` e o tipo de técnica utilizada para gerar o hash do `token` (o valor depois dos : é o _secret_ utilizado para esse _hash_).

Caso tenha perdido essa saída é possível 'reimprimir' utilizando:

```sh
kubeadm token create --print-join-command
```

Agora basta copiar a saída no `node` _worker_ e ele estará no cluster.

<br>


### _Contexto_ 

Ao iniciar um cluster o k8s gerá um arquivo de configurações `admin.conf` no `etc/kubernetes` que armazena as informações de quem é o 'criador/dono' do cluster. 

Sempre que reiniciar uma sessão o k8s irá procurar esse arquivo em uma pasta `.kube/config` que deveria existir na área do usuário em questão.

A criação da pasta, a cópia do arquivo e a alteração de autorização de leitura desse arquivo é o **contexto** mencionado. 

<br>

> Caso isso não seja feito o k8s fica emitindo um erro que diz que ele não consegue comunicação com host (do API server do K8s) na porta 8080 ao iniciar uma nova sessão de usuário. 

<br>

Para evitar esse tipo de erro:

```sh
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

<br>



### Configurando o weave-net como _pod-network controller_

Repare que teoricamente o cluster já está pronto, mas não disponível.

Caso digite:
```sh
kubectl get nodes
```
Verá que todos estão _Not Ready_. Repare que o `pod` do DNS não ficará em _Running_ quando consultar todos os `pods` que estão rodando no cluster com:

```sh
kubectl get pods -n kube-system
```
Isso acontece porque o **k8s** não possui uma rede de comunicação entre os `pods` implementada por padrão, o usuário deve escolher uma e instalar. 

Existem algumas opções disponíveis: [Flannel](https://github.com/coreos/flannel), [Calico](https://docs.projectcalico.org/about/about-calico), [Romana](https://romana.io/) e a nossa opção de uso, o [Weave net](https://www.weave.works/oss/net/).

Para instalar basta no _main_ `node` :

```sh
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
```

Aguarde e repare que ao consultar os `nodes` novamente todos estarão _Ready_.

<br>


### Parabéns! A instalação foi concluída com sucesso. 
