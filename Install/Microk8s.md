## MicroK8s

Essa distribuição é suportada pela [Canonical](https://canonical.com/), mantenedora do Ubuntu, e por um tempo o foco era ser a melhor opção de escolha de pessoas que desejassem um k8s mais leve (são 200MB) e capaz de rodar em dispositivos ARM, como o raspberry pi 4. Atualmente o foco está em atrair os **desenvolvedores** como seus clientes, já contando com praticamente zero operações/configurações para ter um cluster rodando, integração com a CLI de private cloud/virtualização [Multipass](https://multipass.run/), os addons mais populares disponíveis com uma linha de código e suporte empresarial 24/7.

### Especificações Técnicas
O Microk8s usa o `main` node como `worker` também, o que possibilita a instalação do k8s em um único node. **Todavia, utilize esse cenário apenas para o desenvolvimento**. Caso sejam instanciados 3 VMs ou mais o Microk8s replica a API do k8s e banco de dados Dqlite para manter alta disponibilidade, ou seja, caso um node que está eleito como `main` caia ocorre uma eleição para que o outro assuma a posição. 

- 1 ou mais VMS com 1 cpu, 2GB de RAM e 5GB de HD. 

### Instalação
O processo de instalação é feito pelo gerenciador de pacotes Snap do Ubuntu, para isso basta:

```
sudo snap install microk8s --classic
```

### Parabéns! Kubernetes funcionando! 

Porém nesse caso está instanciando como um node apenas, caso queria montar um cluster de 2 ou mais máquinas basta em deles iniciar o join:

```
microk8s add-node
```

E nos outros nodes copiar o comando join que aparece no console. 

### Ajustes
Para a instalação em único node será necessário habilitar a comunicação Pod-to-Pod manualmente, assim:

```
sudo ufw allow in on cni0 && sudo ufw allow out on cni0
sudo ufw default allow routed
```

Para o caso de um cluster com 2 ou mais VMs é possível que seja necessária a inclusão do usuário ubuntu (ou qualquer outro que tenha optado como _default_) no grupo de permissões microk8s antes da adição de novos nodes:

```
sudo usermod -a -G microk8s ubuntu
sudo chown -f -R ubuntu ~/.kube
```

Após o cluster criado (ou o único node instanciado), para testar o funcionamento experimente: 

```
microk8s kubectl create deployment nginx --image=nginx
microk8s kubectl get deploy
```
