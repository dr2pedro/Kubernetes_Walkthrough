## K3s

Essa é a solução peso leve da empresa [Rancher](https://rancher.com/). **Realmente peso leve!! são apenas 60MB de binário**, sem contar que a instalação é facilitada por um `shell` script, semelhante a instalação do Docker. A única restrição encontrada é o pouco conteúdo disponível em comunidade e as documentações com informações reduzidas.

### Especificações Técnicas
O K3s utiliza o `main` node como `worker`, ou seja, é possível fazer uma instalação de desenvolvimento com um único `node`. Em caso de instalação _multi-node_ fique ciente que por padrão não existe alta disponibilidade, ou seja, não terá um `main` node de reserva ou eleição como em outras opções de instalação.

- 1 ou mais VMs com 1 cpu, 1GB de RAM e 5 GB de HD.

### Instalação
Para instalar basta:

```
curl -sfL https://get.k3s.io | sh -
```

### Parabéns, K3s single-node de pé!

### Multi-node

Para a instalação `multi-node`, no node que foi instalado o K3s armazene a informação do IP, da porta 6443 e procure também o token disponibilizado aqui:

```
sudo cat /var/lib/rancher/k3s/server/node-token

---

K10247c5a027153c997e155454993edf654b6778ac6ea2365205fd8845e1d5563515::server:a00efad103a111bd1dd82b0604beff72

```

Agora nos nodes que serão inseridos no cluster digite:

```
curl -sfL https://get.k3s.io | K3S_URL=https://1<IP>:6443 K3S_TOKEN=<TOKEN> sh -
```

Enquanto instala, no `main` node verifique o momento em que os nodes workers ficarão Ready com:

```
kubectl get nodes -w
```

### Parabéns novamente, agora o seu cluster é multi-node. E LEVE! 