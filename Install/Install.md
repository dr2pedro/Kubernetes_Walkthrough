# Instalação do Kubernetes

Justamente por existirem múltiplas formas instanciar um _cluster_ kubernetes é que nesse capítulo serão apresentadas as instalações de algumas delas, levando em consideração o nível de dificuldade de instalação (1-5), a indicação de uso e a uma avaliação pessoal.
<br>

| Distribuição | Dificuldade | Uso | Avaliação Pessoal (0-10) |
|:-------------|:------------|:----|:-------------------------|
| [Vanilla](Vanilla.md) | `****` | produção | 7.5 |
| [Vanilla HA](VanillaHA.md) |  `*****` | produção | 8.5 |
| [MicroK8s](Microk8s.md) | `*` | produção e desenvolvimento | 7.5 |
| [K3s](K3s.md)   | `*` | produção e desenvolvimento | 9.5 |
| [GKE - _user interface_](GKE-ui.md)   |  `**`  | produção  |  9.0
| [GKE - gcloud gdk](GKE-gdk.md)   |  


Ainda nessa seção de instalação existe um capítulo abordando o tema de gerenciamento do Kubectl, também conhecido por alguns como Kube-control, dado que algumas configurações e alguns plugins podem facilitar muito o processo de deploy das aplicações.
