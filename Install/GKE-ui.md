## GKE - user interface

GKE se refere a **Google Kubernetes Engine** e na realidade é a implementação de _cloud_ mais próxima do produto original que se pode ter, dado que o fundador do Kubernetes é a própria Google. 

Vale mencionar que não é, nem de perto, a maior _cloud_ pública para Kubernetes disponível, com essa posição ficando para Amazon Elastic Kubernetes Service (EKS) e logo em seguida a Azure Kubernetes Service (AKS). 

Geralmente opta-se pelo seu uso devido ao calendário de lançamento de novas _features_ do Kubernetes, que são implementadas quase que simultaneamente no GKE. 

Uma outra questão que facilita muitos os usuários é a possibilidade de integrar o serviços pessoais como Google Drive, Fotos, Gmail, Domains e etc... e o GKE em um único perfil de cobrança, o que pode atrair consumidores. 

<br>

> Das três maiores clouds o GKE é a de custo menor, exceto quando consideramos a assinatura por longo prazo da Azure. 

<br>



### Especificações Técnicas
Na realidade os únicos requisitos para a criação de um cluster GKE são um browser e uma conta na Google Cloud Plataform (GCP).

<br>




### Instanciamento
O acesso da GCP se dá pela [_landing page_](https://cloud.google.com/):

![Landing_page](assets/gcp_landing.png)

Após seguir os passos solicitados para o registro ela irá te guiar para o painel da GCP. 

Uma vez nesse painel repare que a toda a estrutura é **orientada a projeto**, ou seja, primeiro você precisa definir um projeto na GCP e nele, dos 246 recursos disponíveis (até a presente data), ativar os que você precisa para esse projeto. 

Repare que no `header` ao lado do logo da GCP se encontra o nome projeto escolhido, caso não tenha selecionado antes, um novo será criado com o nome `My First Project`.

![Panel_page](assets/gcp_panel.png)

Para o período gratuito existe um alerta acima dizendo o quanto ainda resta e os botões caso queira encerrar o período de teste e ativar uma conta paga. **Não clique no `Ativar` antes de consumir todo o crédito do gratuito!!**

Ao clicar na barra de menu do lado esquerdo os atalhos para os grupos de recursos aparecem:

![Menu](assets/gcp_menu.png)

Na seção de `Computação` procure o de nome **Kubernetes Engine**, selecione o pin para prender o atalho como primeira posição e acesse esse recurso, irá aparecer a opção para ativar esse recurso no projeto:

![Active](assets/gcp_active_resource.png)

Na janela irá aparecer um botão com a opção de `Criar`, clique nele.

<br>

> Existe um segundo com a opção apenas de implantar um container, para aplicações menores utilize esse. 

<br>

![Create](assets/gcp_create.png)

Um modal irá aparecer no centro da tela com as escolhas de cluster **Padrão** ou o novo recurso **Piloto Automático**, selecione o padrão.

> O GKE no piloto automático pode ser considerado outra forma de instalação separada. Veremos em um capítulo diferente.

<br>

![Modal](assets/gcp_default.png)

<br>

Um painel de configurações irá aparecer. Existem opções para disponibilidade de cluster em diferentes regiões e de versionamento automático, mas para simplificar, apenas nomeie o cluster e clique no botão criar.

![Config](assets/gcp_config.png)

Aguarde alguns minutos para o instanciamento do cluster e em seguida repare que o cluster está pronto!

![Ready](assets/gcp_ready.png)

### Parabéns! Cluster pronto!

### Ajustes

O primeiro ajuste a ser mecionado é a criação de Labels para o cluster, isso ajuda a diferenciar o propósito de cada recurso. Para isso, selecione o cluster na lista e na aba da direita irá aparecer a opção para `Criar Rótulo`, crie quantos precisar e confirme.

![Labels](assets/gcp_label.png)

Confirme se o label foi corretamente associado ao cluster na lista

![Labels_ok](assets/gcp_label_ok.png)

O segundo ponto importante são as especificações do cluster, acessíveis ao clicar no nome dele na lista. A primeira janela que irá aparecer são as especificações gerais do cluster, algumas opções serão fixas, outras poderão ser alteradas.

![Settings](assets/gcp_settings.png)

Ao clicar na aba Nós será possível ver o tipo de _Pool_ alocado para as VMs e o consumo de cada uma delas

![Nodes](assets/gcp_nodes.png)

Na aba armazenamento, vemos que o cluster ainda não possui nenhum `volume` solicitado, mas já possui três `storage-class` possíveis de serem instanciadas.

![Storage](assets/gcp_storage.png)

E finalmente podemos ver os Logs na aba Registros, repare que é possível filtrar pelo tipo de log.

![Logs](assets/gcp_logs.png)

Esse último ajuste na realidade é uma opção de visualização alternativa ao painel do GCP. Bem semelhante a um perfil de rede social, clique no canto superior direito no logo do GCP e em seguida clique em `Início Visualização`

![Board_2](assets/gcp_board2.png)

E verá que o seguinte _board_ irá aparecer:

![Social_Board](assets/gcp_social_board.png)

### Works pela UI
De volta na página de lista dos clusters no canto superior direito existe a opção de abrir um console na UI, clique nele.

![Console](assets/gcp_console.png)

Vai ver que um console irá aparecer no metade inferior da janela.

![Console2](assets/gcp_console2.png)

No console digite o comando:

```
gcloud container clusters \
    get-credentials [cluster-name] \
    --zone [cluster-zone]
```
Onde o `cluster-name` é o nome que aparece na listagem do cluster que você deseja usar e a `cluster-zone` é a zona em que ele está.

A seguir verifique se a conexão funcinou verificando o número de nodes no cluster:

```
kubectl get nodes
```
Repare que ele trará as informações do cluster corretamente. O próximo recurso, que está sinalizado abaixo e pode ser utilizado para quem deseja fazer `infra-as-code`, que é a opção de abrir uma editor na UI. 

![Editor](assets/gcp_editor.png)

Ao clicar um editor semelhante ao VSCode irá abrir onde estava o console, clique em abrir em uma nova janela para melhor visualização.

![Editor2](assets/gcp_editor2.png)

Repare que o console agora está na parte inferior da tela e o editor no resto da tela. 

![Editor3](assets/gcp_editor3.png)


**Pronto, agora é só começar a criar seus .yaml files!**

### Deletando o Cluster
Essa é a parte **MAIS IMPORTANTE DOS CICLOS DE CLOUDS**, devido a cobrança contínua pelo uso, existe sempre a necessidade de deletar recursos que não são mais necessários e com os clusters k8s não seria diferente. 

Na página da lista dos clusters, selecione o que deseja excluir e clique no botão lofo abaixo do header.

![Delete](assets/gcp_delete.png)

**Pronto, agora o cluster foi deletado.**