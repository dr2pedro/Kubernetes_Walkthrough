## Kubernetes _Vanilla_ HA (_High Available_)

Em Docker quando iniciado um cluster `Swarm` existe uma regra em que se deve ter `(N/2) +1` _main_ `node` onde `N` é número total de `nodes` disponíveis para serem _workes_ ou _main_. Isso garante que se tenha minimamente 2 _main_ nodes em um mínimo de 2 VMs/máquinas disponíveis. 

A essa técnica dá se o nome de `Alta Disponibilidade ou _High Available_ (HA)`, pois o cluster continua existindo em caso de baixas mesmo em configurações mínimas dado que sempre haverá um _main_ node disponível

> Lembre-se o main node é quem contém todos os componentes do Control Plane, ou seja, todos eles serão instanciados em mais de um lugar. 

Então em situações 
- de 7 `nodes` serão: 4 _main_ + 3 _workers_;
- de 5 `nodes`: 3 _main_ + 2 _workers_;
- de 3 `nodes`: 2 _main_ + 1 _workers_;
- de 2 `nodes`: 2 _main_.

O problema é que em **k8s** por _default_ os _main_ `nodes` **NÃO RECEBEM CONTAINERS/PODS**.

<br>

> Isso significa que a disponibilidade de _hardware_ é menor. Então use com parcimônia a HA. O Microk8s por `default` habilita o agendamento Pods nos main nodes, além de utilizar um esquema de "votação" para eleição do _main_ bem similar ao antigo Docker Swarm.  

<br>

Uma adaptação para **k8s** é:

>>  Inicie o seu cluster com a regra mencionada acima e a partir disso insira apenas `nodes` _workers_. Vale lembrar que a adição de nodes em um cluster em produção não é algo rotineiro, o planejamento prévio de consumo de hardware é a estratégia mais comumente usada. 

<br>

Uma outra limitação do K8S para HA é que os _main_ `nodes` são todos ativos mas não alcançáveis por padrão, é necessário um `node` de _Load Balancer_ para gerenciar as requisições para as **API Server**. Nesse caso precisa-se de mais uma VM/máquina que não receberá _containers_/pods.



### Especificações Técnicas HA

- 1 Máquina ou vm com 2GB de RAM, o mínimo de HD e 1 cpu, para instalação do _Load Balancer_ (HAProxy: https://www.haproxy.org/)

- 3 Máquinas ou vms com 4GB de RAM, 20 HD e 2 cpus para os _main_ `nodes`

- 2 ou 3 Máquinas ou vms com 2GB de RAM, 20 HD e 2 cpus.

- SO Linux, Ubuntu focal

- um Container Runtime instalado em todas elas. Dependendo da versão do Kubernetes ainda é possível utilizar o Docker, as versões mais atuais recomendam o uso de outro runtime como o Containerd ou o Podman.

<br>

### Instalação do HAProxy

Devemos iniciar pela instação do _Load Balancer_. Para isso:

```sh
apt update
apt upgrade -y
apt install -y haproxy
```

Na pasta `etc/haproxy` edite o arquivo `haproxy.cfg` e adicione no final dele:

```sh
frontend kubernetes
    mode tcp
    bind IP_DESSA_VM:6443
    option tcplog
    default_backend main_k8s

backend main_k8s
    mode tcp
    balance roundrobin
    option tcp-check
    server main_1 IP_DO_MAIN_1 check fall 3 rise 2
    server main_2 IP_DO_MAIN_2 check fall 3 rise 2
    server main_3 IP_DO_MAIN_3 check fall 3 rise 2
    # são os IPs dos _main_nodes.

```

Depois disso reinicie o HAProxy:

```sh
systemctl restart haproxy
```

Confira se a porta do HAProxy está aberta para conexões:

```sh
netstat -atunp 

# Deve aparecer o IP da VM na porta 6443 aceitando conexões de qualquer lugar (0.0.0.0:*) e status LISTEN.
```

<br>

### Iniciando o cluster _kubernetes_ multimaster

Escolha uma das máquinas que será _main_ `node`, siga os passos da instalação [_Vanilla_](./Vanilla.md) mas não inicie o cluster com o passo do `kubeadm init` daquele modo. Utilize da seguinte forma:

```sh
kubeadm init --control-plane-endpoint "LOAD_BALANCER_DNS:LOAD_BALANCER_PORT" --upload-certs
```

Ele irá imprimir dois tokens, para inserção de _mains_ e para inserção de _workers_. Será mais ou menos parecido com isso (saída exemplo da documentação):

```
...
You can now join any number of control-plane node by running the following command on each as a root:
    kubeadm join 192.168.0.200:6443 --token 9vr73a.a8uxyaju799qwdjv --discovery-token-ca-cert-hash sha256:7c2e69131a36ae2a042a339b33381c6d0d43887e2de83720eff5359e26aec866 --control-plane --certificate-key f8902e114ef118304e561c3ecd4d0b543adc226b7a07f675f56564185ffe0c07
      
Please note that the certificate-key gives access to cluster sensitive data, keep it secret!
As a safeguard, uploaded-certs will be deleted in two hours; If necessary, you can use kubeadm init phase upload-certs to reload certs afterward.
      
Then you can join any number of worker nodes by running the following on each as root:
    kubeadm join 192.168.0.200:6443 --token 9vr73a.a8uxyaju799qwdjv --discovery-token-ca-cert-hash sha256:7c2e69131a36ae2a042a339b33381c6d0d43887e2de83720eff5359e26aec866
```

Insira todos os _mains_ `nodes`, depois todos os workers e por fim siga com a instalação da `pod network` como no capítulo anterior.

### Parabéns, a instalação em alta disponibilidade está concluída!
