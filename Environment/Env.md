## Controle de ambiente
Esse capítulo lida com tudo o que não é Pod (e afins) e nem Services, ou seja, tudo o que pode ser considerado **ambiente**, no sentido geral da palavra.

### Namespaces
Os namespaces podem ser considerados como os ambientes virtuais, eles tem um comportamento de escopo e são capazes de agrupar configurações e recursos. Por exemplo, um kubernetes recém instalado possui pelo menos dois namespaces iniciais, o `kubesystem` e o `default`, os objetos e configurações do `kubesystem` não interferem no escopo `default`, que vem vazio para o uso. 

Vamos começar pela criação de namespaces.

Experimente criar um pod do `nginx` com a flag -n (--namespace) `inicial`:

```
kubectl run nginx --image=nginx --namespace=inicial
kubectl get po -n inicial

```

Repare que ao definir um pod em um namespace que não foi criado o k8s entende e cria automaticamente. Existe a opção de criar namespaces com o `kubectl`.

```
kubectl create namespace segundo
kubectl get namespaces
```

Também é possível utilizar manifestos para criar os namespaces, dessa forma seria:

```json
{
  "kind": "Namespace",
  "apiVersion": "v1",
  "metadata": {
    "name": "terceiro",
    "labels": {
      "name": "terceiro"
    }
  }
}
```

```
kubectl apply -f namespace.json
```

E a última opção é criar através de objetos do tipo kustomize como visto no capítulo de [Ingress](Expose/Expose.md#traefik-ingress) onde existe uma chave para declarar algum namespace já criado ou um novo. Repare que nesse caso, os outros objetos não precisam declarar em que namespace serão implementados, já que o kustomize.yaml já fornece essa informação.

#### Alocando recursos para o namespace
A ideia de ter ambientes virtuais dentro do um cluster de hardware permite maior segregação entre projetos, no entanto, dependendo da dinâmica de trabalho, alguns projetos tendem a consumir mais recursos do que outros, ainda que não seja a intenção de quem os desenvolve. 

Pensando nesse cenário, existe um objeto do kubernetes que se aplica a namespaces de modo que seja possível definir cotas de: **recursos de computação**, como memória ou cpu; **recursos de armazenamento**, como o número de PVCs ou a quantidade de armazenamento de todos os PVCs; **do total de objetos** do kubernetes para aquele namespace como o total de pods, services, configmaps e etc...Para melhor documentação consulte [aqui](https://unofficial-kubernetes.readthedocs.io/en/latest/concepts/policy/resource-quotas/).

Na prática imagine que para um determinado projeto chamado `app-1` você precise limitar:
- o consumo de cpu a 1 com o limite máximo de 2
- a memória de memória a 1Gi com o limite máximo de 2Gi
- o consumo máximo de armazenamento de 2 Gi
- o número de pods em 5
- o número de services em 1
- o número de configmaps em 1
- e o número de PVCs em 4

Vamos iniciar criando o namespace `app-1`

```
kubectl create namespace app-1
```

Agora vamos definir no `cota-objetos.json` o número máximo de objetos, insera o conteúdo a seguir no arquivo.

```json
{
  "kind": "ResourceQuota",
  "apiVersion": "v1",
  "metadata": {
    "name": "cota-objetos"
  },
  "spec": {
    "hard": {
      "pods": 5,
      "services": 1,
      "configmaps": 1
    }
  }
}
```

Aplique, verifique se o objeto foi criado no namespace e consulte o limite utilizado.

```
kubectl apply -f cota-objetos.json -n app-1
kubectl get quota -n app-1
kubectl describe quota/cota-objetos -n app-1

---
Name:       cota-objetos
Namespace:  app-1
Resource    Used  Hard
--------    ----  ----
configmaps  0     1
pods        0     5
services    0     1

```

Agora crie o objeto do tipo `cota-hd.json` e insira o conteúdo a seguir.

```json
{
  "kind": "ResourceQuota",
  "apiVersion": "v1",
  "metadata": {
    "name": "cota-hd"
  },
  "spec": {
    "hard": {
      "requests.storage": "2Gi",
      "persistentvolumeclaims": 4
    }
  }
}
```

Confira se foi criado e verifique o conteúdo.

```
kubectl apply -f cota-hd.json -n app-1
kubectl get quota -n app-1
kubectl describe quota/cota-hd -n app-1

---
Name:                   cota-hd
Namespace:              app-1
Resource                Used  Hard
--------                ----  ----
persistentvolumeclaims  0     4
requests.storage        0     2Gi
```

Por fim, crie as cotas dos recursos computacionais no `cota-recursos.json`.

```json
{
  "kind": "ResourceQuota",
  "apiVersion": "v1",
  "metadata": {
    "name": "cota-recursos"
  },
  "spec": {
    "hard": {
      "cpu": 1,
      "memory": "1Gi",
      "limits.cpu": 2,
      "limits.memory": "2Gi"
    }
  }
}
```

Pronto, agora implemente essa última cota e verifique se o seu ambiente para o app-1 estará montado.

```
kubectl apply -f cota-recursos.json -n app-1
kubectl get quota -n app-1
kubectl describe quota/cota-recursos -n app-1

---
Name:          cota-recursos
Namespace:     app-1
Resource       Used  Hard
--------       ----  ----
cpu            0     1
limits.cpu     0     2
limits.memory  0     2Gi
memory         0     1Gi
```

<br>

#### Gerenciando recursos no namespace
Nessa seção veremos como gerenciar os recursos computacionais e de armazenamento alocados no namespace para uso nos _containers_ e pvcs. Vale mencionar que esse recurso é ideal para estabelecer limites no namespace como um todo, o gerencimento de recursos dentro dos manifestos dos pods ainda será possível de ser feito desde que não viole os limites do namespace.
Ao criar objetos do tipo `LimitRange` no namespace é possível:
- definir valores padrões
- aplicar limites mínimos e máximos


1. Valores padrões

Em manifestos de pods que não contenham nenhuma menção aos recursos utilizados/solicitados o ideal é definir um objeto  `Limit Range` no namespace de modo que todo _container_ ao ser solicitado tenha uma quantidade padrão de recursos destinados.

Crie um arquivo `limite-padrao.json` com o que se deseja de padrão de memória e cpu.

```json
{
  "kind": "LimitRange",
  "apiVersion": "v1",
  "metadata": {
    "name": "limite-mem-cpu"
  },
  "spec": {
    "limits": [
      {         
        "type": "Container",
        "default": {
          "memory": "512Mi"
        },
        "defaultRequest": {
          "memory": "256Mi"
        }
      }
    ]
  }
}
```

E aplique a um namespace.

```
kubectl create namespace meu-namespace
kubectl apply -f limite-padrao.json -n meu-namespace
```

Agora crie um _Pod_ no `meu-namespace` sem os limites...

```
kubectl run nginx --image=nginx -n meu-namespace
```

e verifique se o `LimitRange` aplica essa configuração no pod

```
kubectl get pod nginx --output=json --namespace=meu-namespace
---
## essa parte deve aparecer no .json
        "containers": [
            {
                "image": "nginx",
                "imagePullPolicy": "Always",
                "name": "nginx2",
                "resources": { 
                  "limits": {
                        "memory": "512Mi"
                    },
                    "requests": {
                        "memory": "256Mi"
                    }
                },
                "terminationMessagePath": "/dev/termination-log",
                "terminationMessagePolicy": "File",
                "volumeMounts": [
                    {
                        "mountPath": "/var/run/secrets/kubernetes.io/serviceaccount",
                        "name": "default-token-xssdh",
                        "readOnly": true
                    }
                ]

```

Nesse caso o `requests` é a memória padrão solicitada e o limits é o teto. **Também é possível fazer o mesmo manifesto limitando cpus.**

2. Limites mínimo e máximo
Agora a questão é aplicar um limite máximo e um mínimo, ou seja, o _container_ não será criado a menos que se especifique um valor acima do mínimo e abaixo do máximo permitido.

Crie um objeto do tipo `limite-min-max.json` com o seguinte conteúdo:

```json
{
  "apiVersion": "v1",
  "kind": "LimitRange",
  "metadata": {
    "name": "limite-min-max"
  },
  "spec": {
    "limits": [
      {
        "default": {
          "cpu": 1
        },
        "defaultRequest": {
          "cpu": 0.5
        },
        "type": "Container"
      }
    ]
  }
}
```

Agora aplique ao namespace, verifique se está tudo certo e descreva o objeto.

```
kubectl apply -f limite-min-max.json -n meu-namespace
kubectl get limits -n meu-namespace
kubectl describe limits/limite-min-max -n meu-namespace

---
NAME             CREATED AT
limite-min-max   2021-05-09T12:30:41Z
limite-mem-cpu   2021-05-09T12:26:28Z

Name:       limite-min-max
Namespace:  app-1
Type        Resource  Min  Max  Default Request  Default Limit  Max Limit/Request Ratio
----        --------  ---  ---  ---------------  -------------  -----------------------
Container   cpu       -    -    500m             1              -
```

Também é possível definir valores mínimo e máximo de armazenamento. Crie um arquivo `limite-pvc.json` e aplique o conteúdo a seguir.

```json
{
  "apiVersion": "v1",
  "kind": "LimitRange",
  "metadata": {
    "name": "limite-pvc"
  },
  "spec": {
    "limits": [
      {
        "type": "PersistentVolumeClaim",
        "max": {
          "storage": "2Gi"
        },
        "min": {
          "storage": "1Gi"
        }
      }
    ]
  }
}
```

Aplique e confira o resultado final.

```
kubectl apply -f limite-pvc.json -n meu-namespace
kubectl get limits -n meu-namespace
kubectl describe limits/limite-pvc -n meu-namespace

---
NAME             CREATED AT
limite-min-max   2021-05-09T12:30:41Z
limite-pvc       2021-05-09T12:33:01Z
mem-cpu-de       2021-05-09T12:26:28Z

Name:                  limite-pvc
Namespace:             app-1
Type                   Resource  Min  Max  Default Request  Default Limit  Max Limit/Request Ratio
----                   --------  ---  ---  ---------------  -------------  -----------------------
PersistentVolumeClaim  storage   1Gi  2Gi  -                -              -
```

<br>

> Vale mencionar que os limites consumidos devem estar dentro da cota disponível para o namespace, caso existente. Na situação em que um recurso seja solicitado além da cota, um erro será retornado.

<br>

#### Configurações do namespace
As configurações do ambiente se referem a componentes físicos que precisam estar presentes naquele escopo para as aplicações funcionarem, como arquivos de configurações, variáveis de ambientes, informações sensíveis e autorizações de usuários. Para cada uma dessas coisas existe um objeto específico do kubernetes: ConfigMaps, Secrets e ServiceAccounts.


1. ConfigMaps

Os configmaps, na documentação oficial são indicados para abstrair arquivos de configuração e variáveis de ambiente utilizadas nos builds das imagens dos containers. Todavia, eles podem ser utilizados de mais formas. Vamos passar um por uma.

O primeiro uso do ConfigMap a ser demonstrado é o de armazenar o conteúdo de files como dados. Crie dois arquivos, o `data/file-1.json` e o `data/file-2.json` com:

```json
{
  "classe": "1-B"
}
```
```json
{
  "classe": "2-C"
}
```

Agora crie o ConfigMap com kubectl e aponte para o local que o arquivo se encontra.

```
kubectl create configmap classes-alunos --from-file=data -n meu-namespace
```

Olhe o ConfigMap e será possível ver que os nomes dos files viraram as chamadas dos dados enquanto o seu conteúdo foi considerado como data.

```
kubectl describe configmaps classes-alunos -n meu-namespace
```

Caso na flag `--from-file` aponte apenas para um file invés de uma folder ele também trará o seu conteúdo.
Repare que da forma que está sendo feita, o k8s não considera o conteúdo como variáveis de ambiente, apenas como data, para que isso ocorra bastaria trocar a flag `--from-file` para `--from-env-file`, no entanto, nesse último caso não vai adiantar apontar para um folder, **é necessário apontar para um arquivo em específico de chave a valor (sem ser json, usando igual, sem escape ou aspas)**.

Uma outra opção de tranferir dados de configuração pelo kubectl é diretamente declarando-os na linha de comando pela flag `--from-literal`.

```
kubectl create configmap literal --from-literal=classe=1-C -n meu-namespace
```

Os dados transferidos pelo ConfigMaps podem ser usados dentro de containers. No próximo cenário iremos criar o ConfigMap do nosso `app-1` com a variável de ambiente `ENV:prod`. Para isso crie o `config-env.json` e `pod-env.json`.

```json
{
  "apiVersion": "v1",
  "kind": "ConfigMap",
  "metadata": {
    "name": "config-env",
    "namespace": "app-1"
  },
  "data": {
    "ENV": "prod"
  }
}
```

```json
{
  "apiVersion": "v1",
  "kind": "Pod",
  "metadata": {
    "name": "pod-env"
  },
  "spec": {
    "containers": [
      {
        "name": "test-container",
        "image": "k8s.gcr.io/busybox",
        "command": [
          "/bin/sh",
          "-c",
          "env"
        ],
        "env": [
          {
            "name": "ENV",
            "valueFrom": {
              "configMapKeyRef": {
                "name": "config-env",
                "key": "ENV"
              }
            }
          }
        ]
      }
    ],
    "restartPolicy": "Never"
  }
}
```

Crie o configmap no namespace e em seguida inicie um pod que use essa variável de ambiente.

```
kubectl apply -f config-env.json -n app-1
kubectl apply -f pod-env.json -n app-1
```

Olhe os logs do Pod criado `kubectl logs po/pod-env -n app-1` e verá que dentre as variáveis de ambiente que ele listou estará a ENV=prod. No manifesto o array "env" de dentro do container poderia conter mais de uma varíavel, entretanto, existe uma maneira mais fácil de inserir diversas delas ao mesmo tempo, para isso basta, criar um configmap da seguinte forma:

```json
{
  "apiVersion": "v1",
  "kind": "ConfigMap",
  "metadata": {
    "name": "config-many-env"
  },
  "data": {
    "ENV": "prod",
    "USER": "pedro-dev-01879"
  }
}
```

No pod bastaria trocar a key `valueFrom` para `envFrom` da seguinte forma:

```json
{
  "apiVersion": "v1",
  "kind": "Pod",
  "metadata": {
    "name": "pod-many-env"
  },
  "spec": {
    "containers": [
      {
        "name": "test-container",
        "image": "k8s.gcr.io/busybox",
        "command": [
          "/bin/sh",
          "-c",
          "env"
        ],
        "envFrom": [
          {
            "configMapRef": {
              "name": "config-many-env"
            }
          }
        ]
      }
    ],
    "restartPolicy": "Never"
  }
}
```

Pronto. Agora falaremos sobre o outro uso dos configmaps. Além de guardar informações como dados no próprio objeto é possível montá-lo como um volume para que os dados armazenados se transformem em arquivos novamente no local de montagem. Vamos repetir o nosso o deploy do `data/file-1.json` e `data/file-2.json` como dados só que dessa vez montando como volume nos pods. O configmap `classes` precisa ser feito agora no namespace `app-1`.

```
kubectl create configmap classes --from-file=data -n app-1
```

E crie o `pod-volume.json` com o seguinte conteúdo:

```json
{
  "apiVersion": "v1",
  "kind": "Pod",
  "metadata": {
    "name": "pod-volume"
  },
  "spec": {
    "containers": [
      {
        "name": "test-container",
        "image": "k8s.gcr.io/busybox",
        "command": [
          "/bin/sh",
          "-c",
          "ls /etc/config/"
        ],
        "volumeMounts": [
          {
            "name": "classes",
            "mountPath": "/etc/config"
          }
        ]
      }
    ],
    "volumes": [
      {
        "name": "classes",
        "configMap": {
          "name": "classes"
        }
      }
    ],
    "restartPolicy": "Never"
  }
}
```

```
kubectl apply -f pod.json -n app-1
kubectl logs po/pod-volume -n app-1

---
file-1.json
file-2.json
```

Repare que agora ele lista os dois files inseridos no configMap que agora estão na pasta /etc/config do container. Uma vantagem desse método é que o volume está bindado, ou seja, qualquer alteração feita nos files no root, depois de 1 min, alterará também dentro do container.

<br>

2. Secrets

Algumas informações são consideradas confidenciais, como senhas e tokens de acesso, e não devem ser passadas como variáveis de ambiente ou arquivos de configuração nos ConfigMaps. Orienta-se nesse caso fazer uso do objeto `Secrets`.
Todo o conteúdo de um Secret é convertido em base64 e armazenado no banco de dados do k8s (o etcd). Nessa seção serão explicados 3 tipos de secrets, mas outros existem na [documentação oficial](https://kubernetes.io/docs/concepts/configuration/secret/).

Assim como a maioria dos objetos, também é possível fazê-lo pelo kubectl:

```
kubectl create secret generic usuario --from-literal=user=dr2pedro
```
Repara que o tipo `generic` é denominado um "segredo opaco" que é um dos tipos presentes na documentação. Também é possível fazer por um manifesto.

```json
{
  "kind": "Secret",
  "apiVersion": "v1",
  "metadata": {
    "name": "usuario2"
  },
  "type": "Opaque",
  "data": {
    "user": "dr2p"
  }
}

```
```

kubectl apply -f secret.json -n app-1

```

Um outro tipo de Secret é o padrão o usuário e senha. 

```json
{
   "kind": "Secret",
  "apiVersion": "v1",
  "metadata": {
    "name": "usuario-senha"
  },
  "type": "kubernetes.io/basic-auth",
  "stringData": {
    "username": "admin",
    "password": "t0p-Secret"
  }
}

```
```

kubectl apply -f secret.json -n app-1

```

O último tipo é o armazenamento é o de chaves ssh.

```json
{
  "apiVersion": "v1",
  "kind": "Secret",
  "metadata": {
    "name": "secret-ssh-auth"
  },
  "type": "kubernetes.io/ssh-auth",
  "data": {
    "ssh-privatekey": "MIIEpQIBAAKCAQEAulqb/Y"
  }
}
```

Os segredos são montados como volumes dentro dos containers. A seguir está um template de como seria.

```json
{
  "apiVersion": "v1",
  "kind": "Pod",
  "metadata": {
    "name": "mypod"
  },
  "spec": {
    "containers": [
      {
        "name": "mypod",
        "image": "ubuntu",
        "volumeMounts": [
          {
            "name": "credentials",
            "mountPath": "/etc/.login",
            "readOnly": true
          }
        ]
      }
    ],
    "volumes": [
      {
        "name": "credentials",
        "secret": {
          "secretName": "usuario-senha"
        }
      }
    ]
  }
}
```



<br>

3. ServiceAccount

Essa pode ser uma das partes do kubernetes mais complicadas de entender, pois lida diretamente com segurança, já que é responsável pelo processo de **Autenticação**.

Para isso vamos por partes.

- Todo processo/pod rodando dentro de um _container_ possui um(a) **conta de serviço/ServiceAccount**  para acesso a API do k8s.
- Caso o usuário não especifique no manifesto a conta será a `default` **daquele namespace**.
- Toda `ServiceAccount` cria um secret contendo um JsonWebToken que permite a identificação (autenticação) na API do kubernetes.
- Todo o processo de **Autorização** (vide no próximo capítulo) é dependente de principalmente uma ServiceAccount.

Vamos criar uma `ServiceAccount` para o namespace app-1 e listar as disponíveis.

```
kubectl create serviceaccount front-end -n app-1
kubectl get sa -n app-1
---
NAME        SECRETS   AGE
default     1         15h
front-end   1         45s
```

Agora descreva a serviceaccount `front-end`

```
kubectl describe sa/front-end -n app-1
---
Name:                front-end
Namespace:           app-1
Labels:              <none>
Annotations:         <none>
Image pull secrets:  <none>
Mountable secrets:   front-end-token-nkj4s
Tokens:              front-end-token-nkj4s
Events:              <none>
```

Repare que nas declarações `Tokens` e `Mountable secrets` está escrito o nome do Secret criado. Descreva esse secret para obervar o JWT. 

```
kubectl describe secret/front-end-token-nkj4s -n app-1
---
Name:         front-end-token-nkj4s
Namespace:    app-1
Labels:       <none>
Annotations:  kubernetes.io/service-account.name: front-end
              kubernetes.io/service-account.uid: 9fcba094-c589-435c-98e9-dc0c6429c594

Type:  kubernetes.io/service-account-token

Data
====
ca.crt:     1066 bytes
namespace:  5 bytes
token:      eyJhbGciOiJSUzI1NiIsImtpZCI6ImZtc2dhbDBKdGZMYXF6R0I4bzBKU2Y4TmZuNEhVVHZvdXI0Tm5odHV3LVkifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJhcHAtMSIsImt1YmVybmV0ZXMuaWdennlRC25y2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJmcm9udC1lbmQtdG9rZW4tbmtqNHMiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiZnJvbnQtZW5kIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQudWlkIjoiOWZjYmEwOTQtYzU4OS00MzVjLTk4ZTktZczdc22874YzY0MjljNTk0Iiwic3ViIjoic3lzdGVtOnNlcnZpY2VhY2NvdW50OmFwcC0xOmZyb250LWVuZCJ9.jV9XJdBLIhDWv9SO0VVWyL0v5Rg5Uept_bOSH43RF92b63y5Vz-vup2zPJqAjGVpm5qoCFROb6X-na4ohdXbPUxh_7w7L1SX4kjHYUFXy-9uIZRof1-TTI89KQY6V_HMS8ESiMMCwIxMq8kkI2ncFTVHkLPrrWviW4yGWVf7TdpCVXfaW7MRM_ZFVOBytIPaNnqOinjqYRai2J1V4_YVNvujpNI4PybU6dstlgM-oIAzsNJnnfis0xRWycAqIm9vSItN8BcFfxdK2BVr76Ba9GdpDpPcptanYWO0t1cLF9jYcjXaGX7w3fYhcSMOz41WSZfOD_dUke5BfkwT5JXcqA
```

Agora ao criar o Pod referencie a serviceaccount assim:
```json
{
  "apiVersion": "v1",
  "kind": "Pod",
  "metadata": {
    "name": "pod-volume"
  },
  "spec": {
    "serviceAccountName": "front-end",
    "containers": [
      {
        "name": "test-container",
        "image": "k8s.gcr.io/busybox",
        "command": [
          "/bin/sh",
          "-c",
          "ls /etc/config/"
        ],
        "volumeMounts": [
          {
            "name": "classes",
            "mountPath": "/etc/config"
          }
        ]
      }
    ],
    "volumes": [
      {
        "name": "classes",
        "configMap": {
          "name": "classes"
        }
      }
    ],
    "restartPolicy": "Never"
  }
}
```

Crie o pod e repare ao descrever que o secret está montado.
```
kubectl apply -f pod.json -n app-1
kubectl describe po/pod-volume -n app-1

---

Name:         pod-volume
Namespace:    app-1
Priority:     0
Node:         docker-desktop/192.168.65.4
Start Time:   Mon, 10 May 2021 00:51:03 -0300
Labels:       <none>
Annotations:  kubernetes.io/limit-ranger: LimitRanger plugin set: memory request for container test-container; memory limit for container test-container
Status:       Succeeded
IP:           10.1.0.54
IPs:
  IP:  10.1.0.54
Containers:
  test-container:
    Container ID:  docker://48daaf1bc61fb721fda75648db86a512db1da2f01f022f77a7f27f5e58ef4b41
    Image:         k8s.gcr.io/busybox
    Image ID:      docker-pullable://k8s.gcr.io/busybox@sha256:d8d3bc2c183ed2f9f10e7258f84971202325ee6011ba137112e01e30f206de67
    Port:          <none>
    Host Port:     <none>
    Command:
      /bin/sh
      -c
      ls /etc/config/
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 10 May 2021 00:51:08 -0300
      Finished:     Mon, 10 May 2021 00:51:08 -0300
    Ready:          False
    Restart Count:  0
    Limits:
      cpu:     1
      memory:  512Mi
    Requests:
      cpu:        500m
      memory:     256Mi
    Environment:  <none>
    Mounts:
      /etc/config from classes (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from front-end-token-nkj4s (ro)
Conditions:
  Type              Status
  Initialized       True
  Ready             False
  ContainersReady   False
  PodScheduled      True
Volumes:
  classes:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      classes
    Optional:  false
  front-end-token-nkj4s:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  front-end-token-nkj4s
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  20s   default-scheduler  Successfully assigned app-1/pod-volume to docker-desktop
  Normal  Pulling    18s   kubelet            Pulling image "k8s.gcr.io/busybox"
  Normal  Pulled     15s   kubelet            Successfully pulled image "k8s.gcr.io/busybox" in 2.9878403s
  Normal  Created    15s   kubelet            Created container test-container
  Normal  Started    15s   kubelet            Started container test-container
```
Pronto autenticação concluida, esse pod já é capaz de interagir com a API. 
Lembre-se de deletar a serviceaccount `default` do namespace, já que não será utilizada, ou pelo menos não deveria.

```
kubectl delete sa/default -n app-1
```

Agora vamos ao cerne da questão. A sua aplicação precisa listar pods, services e etc? Criar objetos? Ler segredos?... caso ao refletir por um tempo as respostas sejam não. Então ela não precisa de uma serviceaccount! ou melhor todo pod precisa de uma, mas pelo menos do token ela não vai precisar. Para isso existe a opção de criar a serviceaccount sem o token assim:

```json
{
  "apiVersion": "v1",
  "kind": "ServiceAccount",
  "metadata": {
    "name": "front-end"
  },
  "automountServiceAccountToken": false
}
```

Crie o pod e veja que dessa vez ele não possui o segredo montado. 

<br>

#### Regras para autorizações
Agora iremos ver o processo de **Autorização**, que como dito anteriormente é anexada a uma serviceaccount. No kubernetes existem dois tipos de regras: as `Roles` e as `ClusterRoles`. **Para serviceaccounts não utilize ClusterRules ainda que o conteúdo seja o mesmo**. A partir daqui falaremos apenas das Roles.

Depois da versão 1.6 do Kubernetes (que introduziu a serviceaccount sem token) as roles apenas serão necessárias para aqueles serviços que de fato precisem interagir com o cluster, dentro do seu escopo/namespace.

Vamos iniciar criando um objeto do tipo `Role`.
```json
{
  "kind": "Role",
  "apiVersion": "rbac.authorization.k8s.io/v1",
  "metadata": {
    "namespace": "app-1",
    "name": "basic-role"
  },
  "rules": [
    {
      "apiGroups": [""],
      "resources": ["pods"],
      "verbs": ["get", "watch", "list"]
    }
  ]
}
```

Repare que um objeto do tipo `Role` não possui `spec`, no lugar ele possuir uma chave `rules` que é um array de regras contendo: apigroups, a referência a API do k8s a que a regra se refere; resources, que são os objetos do k8s; e verbos, as ações que podem ser aplicada à esses objetos.

Crie a Role no namespace `app-1`

```
kubectl apply -f role.json -n app-1
kubectl describe -f role.json -n app-1
---
Name:         basic-role
Labels:       <none>
Annotations:  <none>
PolicyRule:
  Resources  Non-Resource URLs  Resource Names  Verbs
  ---------  -----------------  --------------  -----
  pods       []                 []              [get watch list]
```

Por enquanto a regra não está valendo, pois não foi anexada a nenhuma serviceaccount, para isso será necessário criar um objeto do tipo `RoleBinding`.

```json
{
    "kind": "RoleBinding",
    "apiVersion": "rbac.authorization.k8s.io/v1",
    "metadata": {
      "name": "front-end-basic-role"
    },
    "subjects": [
      {
        "kind": "ServiceAccount",
        "name": "front-end"
      }
    ],
    "roleRef": {
      "kind": "Role",
      "name": "front-end",
      "apiGroup": "rbac.authorization.k8s.io"
    }
  }
```
Agora implemente esse objeto no namespace e os pods daquela serviceaccount poderam listar outros pods.
Para todos os verbos confira a documentação oficial. Os principais recursos já foram abordados nesse walkthrough. [Esse blog](https://www.cyberark.com/resources/threat-research-blog/securing-kubernetes-clusters-by-eliminating-risky-permissions) demonstra de forma clara o porquê devemos prestar atenção no processo de autenticação e autorização dos containers.

<br>

### Cluster
Lá no processo de instalação do kubernetes (Vanilla) foi mencionado sobre o `contexto` que nada mais é do que o processo de autenticação do usuário que será usada na CLI para interagir com a API. O usuário padrão é o `root`, mas é possível criar usuários específicos com autorizações específicas para interagir com o cluster.

#### Autenticando um novo usuário

Todo usuário novo criado precisa de um certificado gerado pelo pacote `openssl`.

```
openssl req -new -newkey rsa:4096 -nodes -keyout dr2pedro.key -out dr2pedro.csr -subj "//CN=dr2pedro"
```

Depois disso crie um objeto do tipo `CertificateSigningRequest` da seguinte forma:
```json
{
  "apiVersion": "certificates.k8s.io/v1beta1",
  "kind": "CertificateSigningRequest",
  "metadata": {
    "name": "dr2pedro-crs"
  },
  "spec": {
    "groups": [ "system:authenticated"],
    "request": "TL0ViR3pwWmJSRVgycXpXdEF1MXNLUEd2UTVRQU1tNStjbU4NCndRSURBUUFCb0FBd0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dJQkFIOVpHSFphOVVPZnBIbUpFYWt1aHZvZG1JM2MNCkw2NWFNeXo1bmZDdXRUTUF6MjRtYXZQN3ZuOXZVN25FdlNjWGVldUc4b3NWc3pEdjRXOUxSMk1GcXkxdzRpMEMNCng2S25tMXUwN0tHR1ZyOFZ3RDlmR3YwelRLWUtRVWxMMS90QlZOa3B2ZEdCQ2h3OXkydVE5VHdWTGNRMk5TVEENClVrMGdBeFVyN2ZPNmpyUHpnbVZrUFYyVkpyVyt3RVBDUDRFQWhEdk1BV29uM3Z...vQlVBRjlsWUQ3V2gNClVBMlZuVjVNaWxFeTFvR21saDR",
    "usages": ["client auth"]
  }
}
```

A chave `request` ´é o crs em base 64 que pode ser obtido assim com `cat dr2pedro.csr | base64 | tr -d "\n"`. Confira se o certificado foi criado com:

```
kubectl get csr
---
NAME           AGE     SIGNERNAME                            REQUESTOR            CONDITION
dr2pedro-crs   6m37s   kubernetes.io/kube-apiserver-client   docker-for-desktop   Pending
```

Repare que o _Pending_ indica que o certificado ainda não foi autorizado pela conta root. Para autorizar basta:

```
kubectl certificate approve dr2pedro-crs
kubectl get csr
---
NAME           AGE     SIGNERNAME                            REQUESTOR            CONDITION
dr2pedro-crs   8m26s   kubernetes.io/kube-apiserver-client   docker-for-desktop   Approved,Issued
```

Agora será necessário armazenar os certificados de autorização da conta nova e da "conta root".

```
kubectl get csr dr2pedro-crs -o jsonpath='{.status.certificate}' | base64 --decode > dr2pedro.crt
kubectl config view -o jsonpath='{.clusters[0].cluster.certificate-authority-data}' --raw | base64 --decode - > ca.crt
```

Os próximos passos são um pouco complexos. Inicia setando o cluster com a config da nova conta, depois seta as credenciais apontando para .key de **Autenticação**, em seguida seta o contexto de trabalho (o mesmo feito no processo de instalação) e por fim muda para o novo contexto. Agora o usuário utilizado é o `dr2pedro`.

```
kubectl config set-cluster $(kubectl config view -o jsonpath='{.clusters[0].name}') --server=$(kubectl config view -o jsonpath='{.clusters[0].cluster.server}') --certificate-authority=ca.crt --kubeconfig=dr2pedro-config --embed-certs

kubectl config set-credentials dr2pedro --client-certificate=dr2pedro.crt --client-key=dr2pedro.key --embed-certs --kubeconfig=dr2pedro-config

kubectl config set-context dr2pedro --cluster=$(kubectl config view -o jsonpath='{.clusters[0].name}')  --user=dr2pedro --kubeconfig=dr2pedro-config

kubectl config use-context dr2pedro --kubeconfig=dr2pedro-config

kubectl version --kubeconfig=dr2pedro-config
```

<br>

#### Autorizando o novo usuário
Para autorizar novos usuários utiliza-se os objetos chamados `ClusterRoles`. Como dito anteriormente, essas regras não devem ser utilizadas para autorizar `ServiceAccounts` pois permitiria o controle de todo cluster, enquanto as `Roles` se mantém no contexto do namespace.

A dinâmica das `ClusterRoles` é praticamente igual a das `Roles`, com a vantagem de já existirem algumas delas utilizadas pelo sistema que podem ser atribuidas a novos usuários.

```
kubectl get clusterrole

---
NAME                                                                   CREATED AT
admin                                                                  2021-04-14T02:58:43Z
cluster-admin                                                          2021-04-14T02:58:43Z
edit                                                                   2021-04-14T02:58:43Z
ingress-nginx                                                          2021-05-06T01:58:38Z
ingress-nginx-admission                                                2021-05-06T01:58:39Z
kubeadm:get-nodes                                                      2021-04-14T02:58:45Z
system:aggregate-to-admin                                              2021-04-14T02:58:43Z
system:aggregate-to-edit                                               2021-04-14T02:58:43Z
system:aggregate-to-view                                               2021-04-14T02:58:43Z
system:auth-delegator                                                  2021-04-14T02:58:43Z
system:basic-user                                                      2021-04-14T02:58:43Z
system:certificates.k8s.io:certificatesigningrequests:nodeclient       2021-04-14T02:58:43Z
system:certificates.k8s.io:certificatesigningrequests:selfnodeclient   2021-04-14T02:58:43Z
system:certificates.k8s.io:kube-apiserver-client-approver              2021-04-14T02:58:43Z
system:certificates.k8s.io:kube-apiserver-client-kubelet-approver      2021-04-14T02:58:43Z
system:certificates.k8s.io:kubelet-serving-approver                    2021-04-14T02:58:43Z
system:certificates.k8s.io:legacy-unknown-approver                     2021-04-14T02:58:43Z
system:controller:attachdetach-controller                              2021-04-14T02:58:43Z
system:controller:certificate-controller                               2021-04-14T02:58:43Z
system:controller:clusterrole-aggregation-controller                   2021-04-14T02:58:43Z
system:controller:cronjob-controller                                   2021-04-14T02:58:43Z
system:controller:daemon-set-controller                                2021-04-14T02:58:43Z
system:controller:deployment-controller                                2021-04-14T02:58:43Z
system:controller:disruption-controller                                2021-04-14T02:58:43Z
system:controller:endpoint-controller                                  2021-04-14T02:58:43Z
system:controller:endpointslice-controller                             2021-04-14T02:58:43Z
system:controller:endpointslicemirroring-controller                    2021-04-14T02:58:43Z
system:controller:expand-controller                                    2021-04-14T02:58:43Z
system:controller:generic-garbage-collector                            2021-04-14T02:58:43Z
system:controller:horizontal-pod-autoscaler                            2021-04-14T02:58:43Z
system:controller:job-controller                                       2021-04-14T02:58:43Z
system:controller:namespace-controller                                 2021-04-14T02:58:43Z
system:controller:node-controller                                      2021-04-14T02:58:43Z
system:controller:persistent-volume-binder                             2021-04-14T02:58:43Z
system:controller:pod-garbage-collector                                2021-04-14T02:58:43Z
system:controller:pv-protection-controller                             2021-04-14T02:58:43Z
system:controller:pvc-protection-controller                            2021-04-14T02:58:43Z
system:controller:replicaset-controller                                2021-04-14T02:58:43Z
system:controller:replication-controller                               2021-04-14T02:58:43Z
system:controller:resourcequota-controller                             2021-04-14T02:58:43Z
system:controller:route-controller                                     2021-04-14T02:58:43Z
system:controller:service-account-controller                           2021-04-14T02:58:43Z
system:controller:service-controller                                   2021-04-14T02:58:43Z
system:controller:statefulset-controller                               2021-04-14T02:58:43Z
system:controller:ttl-controller                                       2021-04-14T02:58:43Z
system:coredns                                                         2021-04-14T02:58:45Z
system:discovery                                                       2021-04-14T02:58:43Z
system:heapster                                                        2021-04-14T02:58:43Z
system:kube-aggregator                                                 2021-04-14T02:58:43Z
system:kube-controller-manager                                         2021-04-14T02:58:43Z
system:kube-dns                                                        2021-04-14T02:58:43Z
system:kube-scheduler                                                  2021-04-14T02:58:43Z
system:kubelet-api-admin                                               2021-04-14T02:58:43Z
system:node                                                            2021-04-14T02:58:43Z
system:node-bootstrapper                                               2021-04-14T02:58:43Z
system:node-problem-detector                                           2021-04-14T02:58:43Z
system:node-proxier                                                    2021-04-14T02:58:43Z
system:persistent-volume-provisioner                                   2021-04-14T02:58:43Z
system:public-info-viewer                                              2021-04-14T02:58:43Z
system:volume-scheduler                                                2021-04-14T02:58:43Z
view                                                                   2021-04-14T02:58:43Z
vpnkit-controller                                                      2021-04-14T03:00:05Z
```

Vamos associar role `edit` ano novo usuário `dr2pedro`. Para isso, descreva o que ela faz.

```
kubectl describe clusterrole edit

---

Name:         edit
Labels:       kubernetes.io/bootstrapping=rbac-defaults
              rbac.authorization.k8s.io/aggregate-to-admin=true
Annotations:  rbac.authorization.kubernetes.io/autoupdate: true
PolicyRule:
  Resources                                    Non-Resource URLs  Resource Names  Verbs
  ---------                                    -----------------  --------------  -----
  configmaps                                   []                 []              [create delete deletecollection patch update get list watch]      
  endpoints                                    []                 []              [create delete deletecollection patch update get list watch]      
  persistentvolumeclaims                       []                 []              [create delete deletecollection patch update get list watch]      
  pods                                         []                 []              [create delete deletecollection patch update get list watch]      
  replicationcontrollers/scale                 []                 []              [create delete deletecollection patch update get list watch]      
  replicationcontrollers                       []                 []              [create delete deletecollection patch update get list watch]      
  services                                     []                 []              [create delete deletecollection patch update get list watch]      
  daemonsets.apps                              []                 []              [create delete deletecollection patch update get list watch]      
  deployments.apps/scale                       []                 []              [create delete deletecollection patch update get list watch]      
  deployments.apps                             []                 []              [create delete deletecollection patch update get list watch]      
  replicasets.apps/scale                       []                 []              [create delete deletecollection patch update get list watch]      
  replicasets.apps                             []                 []              [create delete deletecollection patch update get list watch]      
  statefulsets.apps/scale                      []                 []              [create delete deletecollection patch update get list watch]      
  statefulsets.apps                            []                 []              [create delete deletecollection patch update get list watch]      
  horizontalpodautoscalers.autoscaling         []                 []              [create delete deletecollection patch update get list watch]      
  cronjobs.batch                               []                 []              [create delete deletecollection patch update get list watch]      
  jobs.batch                                   []                 []              [create delete deletecollection patch update get list watch]      
  daemonsets.extensions                        []                 []              [create delete deletecollection patch update get list watch]      
  deployments.extensions/scale                 []                 []              [create delete deletecollection patch update get list watch]      
  deployments.extensions                       []                 []              [create delete deletecollection patch update get list watch]      
  ingresses.extensions                         []                 []              [create delete deletecollection patch update get list watch]      
  networkpolicies.extensions                   []                 []              [create delete deletecollection patch update get list watch]      
  replicasets.extensions/scale                 []                 []              [create delete deletecollection patch update get list watch]      
  replicasets.extensions                       []                 []              [create delete deletecollection patch update get list watch]      
  replicationcontrollers.extensions/scale      []                 []              [create delete deletecollection patch update get list watch]      
  ingresses.networking.k8s.io                  []                 []              [create delete deletecollection patch update get list watch]      
  networkpolicies.networking.k8s.io            []                 []              [create delete deletecollection patch update get list watch]      
  poddisruptionbudgets.policy                  []                 []              [create delete deletecollection patch update get list watch]      
  deployments.apps/rollback                    []                 []              [create delete deletecollection patch update]
  deployments.extensions/rollback              []                 []              [create delete deletecollection patch update]
  pods/attach                                  []                 []              [get list watch create delete deletecollection patch update]      
  pods/exec                                    []                 []              [get list watch create delete deletecollection patch update]      
  pods/portforward                             []                 []              [get list watch create delete deletecollection patch update]      
  pods/proxy                                   []                 []              [get list watch create delete deletecollection patch update]      
  secrets                                      []                 []              [get list watch create delete deletecollection patch update]      
  services/proxy                               []                 []              [get list watch create delete deletecollection patch update]      
  bindings                                     []                 []              [get list watch]
  events                                       []                 []              [get list watch]
  limitranges                                  []                 []              [get list watch]
  namespaces/status                            []                 []              [get list watch]
  namespaces                                   []                 []              [get list watch]
  persistentvolumeclaims/status                []                 []              [get list watch]
  pods/log                                     []                 []              [get list watch]
  pods/status                                  []                 []              [get list watch]
  replicationcontrollers/status                []                 []              [get list watch]
  resourcequotas/status                        []                 []              [get list watch]
  resourcequotas                               []                 []              [get list watch]
  services/status                              []                 []              [get list watch]
  controllerrevisions.apps                     []                 []              [get list watch]
  daemonsets.apps/status                       []                 []              [get list watch]
  deployments.apps/status                      []                 []              [get list watch]
  replicasets.apps/status                      []                 []              [get list watch]
  statefulsets.apps/status                     []                 []              [get list watch]
  horizontalpodautoscalers.autoscaling/status  []                 []              [get list watch]
  cronjobs.batch/status                        []                 []              [get list watch]
  jobs.batch/status                            []                 []              [get list watch]
  daemonsets.extensions/status                 []                 []              [get list watch]
  deployments.extensions/status                []                 []              [get list watch]
  ingresses.extensions/status                  []                 []              [get list watch]
  replicasets.extensions/status                []                 []              [get list watch]
  ingresses.networking.k8s.io/status           []                 []              [get list watch]
  poddisruptionbudgets.policy/status           []                 []              [get list watch]
  serviceaccounts                              []                 []              [impersonate create delete deletecollection patch update get list 
watch]
```
Para os principais objetos do k8s essa role apenas tem os verbos `get`, `list` e `watch`. Atribua ela ao usuário com:

```
kubectl create rolebinding edit-dr2pedro --role=edit --user=dr2pedro
```

Feito! O usuário ficará restrito a essas ações. 