# Deploy das aplicações 

Existe mais de uma maneira de fazer o _deploy_ das aplicações no **k8s**. Nessa seção vamos entender a diferença entre _Pod_ e _containers_, a hierárquia presente nos _Deployments_, os casos de uso de um _Daemonset_, um _Deployment_, um _Statefulset_, um _Job_ e uma _CronJob_, o gerenciamento de volumes e de _Pods_. 

<br>

## Os _Pods_ são containers?

A unidade fundamental de um _Container Runtime_ como o Docker é o próprio _container_. Segundo o [site oficial](https://www.docker.com/resources/what-container) podemos considerar que um container é: 

<br>

> Uma unidade padronizada (configurações fixas) de um _software_ contendo todo o seu código e suas dependências, de modo que possa ser executado em diferentes ambientes computacionais de forma **rápida** e **confiável**.

<br>

O Docker utiliza o _kernel_ do _host_ para gerenciar esse isolamento, assim o esquema de funcionamento é: 

![schema-1](assets/schema-1.png)

<br>

Quando uma ou mais "unidades de _softwares_" compartilham configurações e/ou dependências, ou seja, não podem existir isoladas, no **Docker** elas seriam incluídas em um mesmo _container_, já no **Kubernetes** elas podem existir cada uma em seu _container_ mas dentro de um mesmo **Pod**.

<br>

> É importante lembrar que o **k8s não cria os _containers_**, apenas define os **Pods**, sendo assim, o **k8s** ainda necessita de um _container runtime_ que faça isso por ele (como o **Docker**).

<br>

Basicamente o que o **k8s** faz é **isolar dentro de mesmo endereço de IP** todos os _containers_ que possuam as mesmas dependências. O esquema de funcionamento é:

![schema-2](assets/schema-2.svg)

Por fim, podemos dizer: **Sim, os Pods são um conjunto de 1 ou mais _containers_, junto de suas dependências e configurações, sob um mesmo endereço de IP**.

<br>

## A tríade Pod + Replicaset + Deployment

Todos os objetos no **k8s** são criados por meio de **manifestos** que podem ser escritos como arquivos do tipo `.yaml` ou do tipo `.json`, esse último terá a seguinte estrutura básica:

```json
{
    "kind": "",
    "apiVersion": "",
    "metadata": {},
    "spec": {}
}
```

- **`kind`**: representa o objeto que será criado pela API do **k8s**;

- **`apiVersion`**: qual API (_veja abaixo_) do **k8s** que irá interpretar esse objeto. 

<br>

> A API do **k8s** foi sendo implementada em pastas separadas ao longo das atualizações. Cada pasta é considerada uma API e é responsável pela criação de um grupo de objetos.

<br>

Nessa sessão as API utilizadas serão: 
1. `v1` para os **Pods**
2. `apps/v1` para os **Replicasets** e os **Deployments**

<br>

- **`metadata`**: dados que indentificam o objeto a ser criado, que podem ser o `name`, as labels, as `annotations` e etc..

- **`spec`**: as especificações de fato do objeto. 

<br>

Dito isso, agora será necessário entender o por quê vamos falar dos três objetos juntos.

No **k8s** existe uma "hierarquia" de implementação de alguns grupos de objeto. Isso significa que para implementar um objeto de um nível acima ele precisa conter em seu **manifesto** o objeto do nível anterior. Esse é exatamente o caso dos _Pod_, _Replicaset_ e _Deployment_. 

<br>

### Criando o primeiro Pod

Para iniciar, vamos criar um _Pod_ com o recurso `dry-run` que gera um arquivo, no nosso caso em `.json`, mas não gera o pedido para criação na API do **k8s**:

```sh
kubectl run nginx --dry-run=client -o json --image=nginx > pod-template.json

cat pod-template.json
```

Podemos ver a estrutura do _file_:

```json
{
    "kind": "Pod",
    "apiVersion": "v1",
    "metadata": {
        "name": "nginx",
        "creationTimestamp": null,
        "labels": {
            "run": "nginx"
        }
    },
    "spec": {
        "containers": [
            {
                "name": "nginx",
                "image": "nginx",
                "resources": {}
            }
        ],
        "restartPolicy": "Always",
        "dnsPolicy": "ClusterFirst"
    },
    "status": {}
}
```

Os campos `kind` e `apiVersion` já foram explicados. 

No `metadata` temos: a `label` que será utilizada nos objetos de nível superior para identificar em que _Pod_ ele precisará aplicar suas especificações; e o `name`, que é o nome designado ao _Pod_. Vale mencionar que esse **não é o nome da imagem e nem do container!** 

Nas especificações (`spec`) temos a declaração dos `"containers"` no qual o seu valor é um _array_ de objetos, cada um contendo as declarações sobre um _container_, que minimamente deve conter as chaves: `"name"` e `"image"`. **Aqui é o nome do container e o nome da imagem no Docker Hub**. 

Repare que por padrão o argumento `"resources"` é um objeto vazio. Entretanto é comum que se defina o quanto de recursos de _hardware_ será alocado aos _containers_ (vide seção "Gerenciamento de _Pods_ capítulo _Limitando recursos no Pod_).

Como já dito anteriormente o _Pod_ determina um IP único para todos os containers dentro dele. Podemos conferir esse IP com o método `describe` indicando o nome do _Pod_:

```
kubectl describe po nginx

```
![terminal-0](assets/terminal-0.png)


<br>

### Criando o primeiro _ReplicaSet_ 

Um _ReplicaSet_ é o **controlador** dos _Pods_, com ele é possível determinar a quantidade de _Pods_ que quereremos ativos. O esquema de um _Replicaset_ é esse:

![schema-3](assets/schema-3.svg)


A estrutura do _.json_ do _ReplicaSet_ é de um _Pod_ dentro do _Replicaset_, vejamos:

1. Pega-se a parte `spec` de um Pod:

```json
"spec": {
        "containers": [
            {
                "name": "ubuntu",
                "image": "nginx",
                "args": [
                    "bin/sh",
                    "-c",
                    "echo Hello \"$name\";sleep 36000"
                ],
                "env": [
                    {
                        "name": "name",
                        "value": "Bob"
                    }
                ],
                "resources": {}
            }
        ],
        "restartPolicy": "Always",
        "dnsPolicy": "ClusterFirst"
    }
```

2. Pegam-se as labels que o _Pod_ aplica nos containers:
```json
"metadata": {
        "labels": {
            "run": "ubuntu"
        }
```

3. Preenche-se a estrutura básica de um objeto do tipo _Replicaset_, que contém três campos obrigatórios na declaração `"spec"`: 

```json
{
    "kind": "Replicaset",
    "apiVersion": "apps/v1",
    "metadata": {
        "name": "meuprimeiroreplicaset"
    },
    "spec": {
        "replicas": "",
        "selector": {},
        "template": {}
    }
}
```

* `replicas`: o número de quantos _Pods_ daquele serão necessários para o _Replicaset_ cumprir a tarefa dele;
* `selector`: o que será usado para identificar os _containers_ que o _Replicaset_ é responsável;
* `template`: a estrutura do _Pod_ que foi separada nos passos anteriores.

4. Juntando todas as partes, fica assim:

```json
{
    "kind": "ReplicaSet",
    "apiVersion": "apps/v1",
    "metadata": {
        "name": "meuprimeiroreplicaset"
    },
    "spec": {
        "replicas": 3,
        "selector": {
            "matchLabels": {
                "run": "ubuntu"
            }
        },
        "template": {
            "metadata": {
                "labels": {
                    "run": "ubuntu"
                }
            },
            "spec": {
                "containers": [
                        {
                        "name": "ubuntu",
                        "image": "ubuntu",
                        "args": [
                            "bin/sh",
                            "-c",
                            "echo Hello \"$name\";sleep 36000"
                        ],
                        "env": [
                            {
                                "name": "name",
                                "value": "Bob"
                            }
                        ],
                        "resources": {}
                        }
                    ],
                "restartPolicy": "Always",
                "dnsPolicy": "ClusterFirst"
            }    
        }
    }
}
```
Salve esse objeto em um .json e crie com o `kubectl create --filename` _nome do objeto_.

![terminal-3](assets/terminal-3.png)

Confira o número de _Pods_ presente:

```
kubectl get replicaset


NAME                    DESIRED   CURRENT   READY   AGE
meuprimeiroreplicaset   3         3         3       77m

```

Ao atingir o número de _Pods_ determinado o _Replicaset_ cumpre sua tarefa, entretanto ele ainda se mantém ativo uma vez que caso um _Pod_ seja deletado ele é o responsável por levantar um novo. 

Vamos testar isso. Repare que abaixo o meu _Replicaset_ criou 3 _Pods_: 

![terminal-4](assets/terminal-4.png)

Todos possuem mais de 50 minutos de vida. Vou deleter um deles e checar se o _Replicaset_ vai repor.

```
kubectl delete po meuprimeiroreplicaset-c2dt4
kubectl get po
```
![terminal-5](assets/terminal-5.png)

**Repare que agora eu tenho um _Pod_ de 46 segundos de vida!**

Vamos escalar esse _Replicaset_ solicitando 10 replicas do _Pod_.

```
kubectl scale replicaset meuprimeiroreplicaset --replicas=10
```

Agora veja em quais `nodes` esses _Pods_ estão rodando

```
kubectl get po -o wide
```
![terminal-6](assets/terminal-6.png)

**São 5 replicas rodando na vm-02 e 5 na vm-03**

> Lembre-se que por _default_ o node `master` não recebe _Pods_ que não sejam do sistema. 


<br>


### Criando o primeiro _Deployment_

O _Deployment_ é o **controlador** do _Replicaset_, com ele é possível armazenar 1 ou mais _Replicasets_. O esquema de funcionamento é esse: 

![schema-4](assets/schema-4.png)

A estrutura do `.json` é muito semalhante a do _Replicaset_ apenas sendo necessário trocar a declaração `"kind"` para **"Deployment"** e o `"name"` caso seja necessário. Dessa vez vamos pedir 10 replicas.

```json
{
    "kind": "Deployment",
    "apiVersion": "apps/v1",
    "metadata": {
        "name": "meuprimeirodeploy"
    },
    "spec": {
        "replicas": 10,
        "selector": {
            "matchLabels": {
                "run": "ubuntu"
            }
        },
        "template": {
            "metadata": {
                "labels": {
                    "run": "ubuntu"
                }
            },
            "spec": {
                "containers": [
                        {
                        "name": "ubuntu",
                        "image": "ubuntu",
                        "args": [
                            "bin/sh",
                            "-c",
                            "echo Hello \"$name\";sleep 36000"
                        ],
                        "env": [
                            {
                                "name": "name",
                                "value": "Bob"
                            }
                        ],
                        "resources": {}
                        }
                    ],
                "restartPolicy": "Always",
                "dnsPolicy": "ClusterFirst"
            }    
        }
    }
}
```

Crie esse objeto e use o `describe` para entender o seu funcionamento:

```
kubectl create --filename deployment-template.json

kubectl describe deploy meuprimeirodeploy

```

![terminal-6](assets/terminal-7.png)

**Repare na seta vermalha, os dois últimos _Replicasets_ criados por esse _Deployment_ ficam expostos no describe** durante o momento de transição.
<br>

Vamos fazer essa troca. 
1. Altere o **manifesto** do seu _Deployment_ para imagens do `nginx` mas não troque as _labels_.

```json
{
    "kind": "Deployment",
    "apiVersion": "apps/v1",
    "metadata": {
        "name": "meuprimeirodeploy"
    },
    "spec": {
        "replicas": 10,
        "selector": {
            "matchLabels": {
                "run": "ubuntu"
            }
        },
        "template": {
            "metadata": {
                "labels": {
                    "run": "ubuntu"
                }
            },
            "spec": {
                "containers": [
                        {
                        "name": "nginx",
                        "image": "nginx",
                        "args": [
                            "bin/sh",
                            "-c",
                            "echo Hello \"$name\";sleep 36000"
                        ],
                        "env": [
                            {
                                "name": "name",
                                "value": "Bob"
                            }
                        ],
                        "resources": {}
                        }
                    ],
                "restartPolicy": "Always",
                "dnsPolicy": "ClusterFirst"
            }    
        }
    }
}
```
2. Edite o deploy com o método `apply`

```
kubectl apply --filename deploy-template.json

```

3. (Seja rápido) Peça para descrever o _Deployment_

```
kubectl describe deploy meuprimeirodeploy
```

4. Repare na parte `OldReplicaSets` e `NewReplicaSet`.

![terminal-8](assets/terminal-8.png)

**Ele cria 2 _Pods_ (25% de max surge) da nova imagem enquanto deleta os outros 2 da imagem antiga (25% max unavailable).**

<br>

>Essa técnica é conhecida como _Rollout_, equivalente a um _deploy_ de uma nova versão da aplicação que já estava em produção. 

**Vale mencionar que:** Como vimos anteriormente o escalonamento (`scale`) é feito a nível de _Replicaset_, ou seja, ao escalar um _Deployment_ ele não cria um novo _Replicaset_ apenas transfere esse pedido para o que estiver em execução.

É possível verificar quantas versões do _Deployment_ existem com o método `rollout history`.

```
kubectl rollout history deploy meuprimeirodeploy
```
Para informações mais detalhadas informe a `REVISION` que quer ver:

```
kubectl rollout history deploy meuprimeirodeploy --revision=1
```
**Ele descreve que a imagem anterior ao `apply` que fizemos era do `ubuntu` e não do `nginx` que é o atual estado**

Vamos retornar o _Deployment_ para a imagem do `ubuntu` e ver se a imagem alterou descrevendo o _Deployment_.

```
kubectl rollout undo deploy meuprimeirodeploy --to-revision=1
kubectl describe deploy meuprimeirodeploy | grep -i image:
```
![terminal-9](assets/terminal-9.png)

<br>

> Essa técnica é conhecida como _RollBack_, útil quando em produção o deploy de uma nova versão da aplicação quebrou alguma coisa e é necessário voltar a versão "estável".

<br>

As duas propriedades `max surge` e `max unavailable`podem ser controladas com a declaração `"strategy"` nas especificações do _Deployment_. Vamos editar o nosso arquivo `.json` para permitir que até 5 _Pods_ fiquem indisponíveis e até 2 novos possam surgir no momento do _Rollout/RollBack_.

```json
{
    "kind": "Deployment",
    "apiVersion": "apps/v1",
    "metadata": {
        "name": "meuprimeirodeploy"
    },
    "spec": {
        "replicas": 10,
        "selector": {
            "matchLabels": {
                "run": "ubuntu"
            }
        },
        "template": {
            "metadata": {
                "labels": {
                    "run": "ubuntu"
                }
            },
            "spec": {
                "containers": [
                        {
                        "name": "nginx",
                        "image": "nginx",
                        "args": [
                            "bin/sh",
                            "-c",
                            "echo Hello \"$name\";sleep 36000"
                        ],
                        "env": [
                            {
                                "name": "name",
                                "value": "Bob"
                            }
                        ],
                        "resources": {}
                        }
                    ],
                "restartPolicy": "Always",
                "dnsPolicy": "ClusterFirst"
            }    
        },
        "strategy": {
            "type": "RollingUpdate",
            "rollingUpdate": {
                "maxUnavailable": 5,
                "maxSurge": 2
            }
        }
    }
}
```
```
kubectl create --filename deploy-template.json
kubectl describe deploy meuprimeirodeploy
```

Repare que agora os valores do `"RollingUpdateStrategy"` mudaram. Experimente fazer um `"Rollout/RollBack"` como explicado anteriormente e verifique os eventos com o método `describe` para ver as alterações funcionando. 

<br>

## Persistindo dados

A criação de _Volumes_ é parte essencial do uso de _containers_ em produção, é com eles que vamos preservar informações que serão utilizadas entre serviços ou armazenadas para consultas de caráter histórico. 

Em todos os _containers runtime_ eles são entidades separadas dos próprios _containers_, e trabalham por meio de vinculação, havendo permissões de `escrita/leitura` ou apenas `leitura`. 
O **k8s** apresenta algumas opções de criação de _volumes_, nessa seção vamos avaliá-las.

<br>

### Gerenciando dados temporários com EmptyDir
O _EmptyDir_ é a única opção de montar um _volume_ que não persiste dados após o _Pod_ deletado. 

O _volume_ é criado assim que os _containers_ do _Pod_ são criados, os dados são compartilhados entre esses _containers_ e existirão no `node` em que o _Pod_ foi designado até o momento de sua deleção.

<br>

> A excessão a esse cenário é quando o _Pod_ apresenta algum tipo de erro, nesse caso, o _volume_ continua existindo para persistir os dados para o próximo _Pod_.

<br>

Geralmente essa opção de "persistência de dados" temporária é utilizada para armazenar dados de _Logs_. 

Vamos incluir um _EmptyDir_ em nosso _Pod_.

1. Do último arquivo de _Deployment_ pegue a parte referente ao _Pod_ (a `"spec"` acima do `"containers"`), dentro do _container_ adicione a declaração "volumeMounts" (que é um _array_) e fora do _container_ crie o `"volumes"` (outro array) assim:

```json
        "spec": {
                "containers": [
                        {
                        "name": "nginx",
                        "image": "nginx",
                        "args": [
                            "bin/sh",
                            "-c",
                            "echo Hello \"$name\";sleep 36000"
                        ],
                        "env": [
                            {
                                "name": "name",
                                "value": "Bob"
                            }
                        ],
                        "resources": {},
                        "volumeMounts": [
                            {
                            "name": "meu-diretorio-vazio",
                            "mountPath": "/meu-direotrio-vazio"
                            }
                        ]
                    }
                ],
                "restartPolicy": "Always",
                "dnsPolicy": "ClusterFirst",
                "volumes": [
                    {
                    "name": "meu-diretorio-vazio",
                    "emptyDir": {}
                    }
                ]
            }
```

2. Substitua essa parte no seu arquivo _Deployment_ e crie com o `kubectl create`

```json
{
    "kind": "Deployment",
    "apiVersion": "apps/v1",
    "metadata": {
        "name": "meuprimeirodeploy"
    },
    "spec": {
        "replicas": 10,
        "selector": {
            "matchLabels": {
                "run": "ubuntu"
            }
        },
        "template": {
            "metadata": {
                "labels": {
                    "run": "ubuntu"
                }
            },
            "spec": {
                "containers": [
                        {
                        "name": "nginx",
                        "image": "nginx",
                        "args": [
                            "bin/sh",
                            "-c",
                            "echo Hello \"$name\";sleep 36000"
                        ],
                        "env": [
                            {
                                "name": "name",
                                "value": "Bob"
                            }
                        ],
                        "resources": {},
                        "volumeMounts": [
                            {
                            "name": "meu-diretorio-vazio",
                            "mountPath": "./emptydir"
                            }
                        ]
                    }
                ],
                "restartPolicy": "Always",
                "dnsPolicy": "ClusterFirst",
                "volumes": [
                    {
                    "name": "meu-diretorio-vazio",
                    "emptyDir": {}
                    }
                ]
            }    
        },
        "strategy": {
            "type": "RollingUpdate",
            "rollingUpdate": {
                "maxUnavailable": 5,
                "maxSurge": 2
            }
        }
    }
}
```
```
kubectl create --filename deploy-template.json
```

3. Liste os _Pods_ e veja em qual `node` eles foram alocados, escolha um e liste o conteúdo do _container_ nginx. 

```
kubectl get po -o wide
kubectl -it exec meuprimeirodeploy-987dcbb64-8sbsf -c nginx -- ls
```
**Repare que possuímos uma pasta chamada "emptydir", o que indica que nosso volume foi montado corretamente.**

Experimente ir no `node` que possui esse _Pod_ e liste o conteúdo da pasta `var/lib/kubelet/pods/<<ID_DO_POD>>/volumes/` e a pasta `emptydir` aparecerá assim: **kubernetes.io~empty-dir**. Ela exitirá nesse `node` até o momento em que ele não tiver mais _Pods_ que a utilizem.

<br>

### Persistência de dados com Persistent Volume (PV) e Persistent Volume Claim (PVC)

Vimos que o EmptyDir é criado automaticamente quando declarado dentro do **manifesto** do _Pod_. Esse não é o caso do _Persistent Volume_ (PV), uma vez que ele exige o seu próprio **manifesto**.

Inicialmente será necessário instalar um driver de _volumes_ para rede, um modo de compartilhar entre diferentes VMs um diretório presente em uma delas, existem diversas opções como: NFS, Flocker, Glusterfs e etc...([vide documentação](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)). 

<br>

#### NSF Server

Para instalar o **NFS Server** no `main node`, basta:

**(root access)**
```
apt install -y nfs-kernel-server
mkdir /opt/dados
chmod 1777 /opt/dados/
```

Edite o `/etc/exports` e adicione a seguinte linha:
```
/opt/dados *(rw,sync,no_root_squash,subtree_check)
```

Aplique as novas configurações e reinicie o serviço:
```
exportfs -a
systemctl restart nfs-kernel-server
```

**Nos demais `nodes`** instale e procure a pasta compartilhada pelo NFS:
```
apt install -y nfs-common
showmout -e <<IP do master node>>
```

<br>

#### GlusterFS 

Repita o passo abaixo em todos _nodes_ (mínimo 2) que serão considerados _servers_:

**(root access)**
```sh
apt update 
apt upgrade
apt install -y software-properties-common
add-apt-repository ppa:gluster/glusterfs-7
apt install -y glusterfs-server
systemctl start glusterd
systemctl enable glusterd
service glusterd status
```

**A documentação aconselha que se crie uma partição separada em um disco vazio para trabalhar com o GlusterFS**. Em produção isso deve ser feito, mas para o desenvolvimento vamos criar na partição em que estamos e replicar em todos os _nodes_ os dados.

Caso tenha mais de um _node_ conecte todos eles  e crie as pastas que irão armazenar os dados dos volumes com:


```sh
sudo gluster peer probe <nome-ou-IP>

sudo mkdir -p /data/glusterfs/brick1/gv0

```

Agora crie o volume com:

```sh
sudo gluster volume create <nome-do-volume> replica <número-de-nodes> transport tcp <ip-node1>:<pasta> <ip-node2>:<pasta> force

## caso tenha dois nodes seria assim:

sudo gluster volume create gv0 replica 2 IP:/data/glusterfs/brick1/gv0 IP:/data/glusterfs/brick1/gv0 force
```

<br>

> Em partições separadas, referencie o PATH para essa partição em cada IP dos nodes e tire a palavra force no final.

<br>

**Execute apenas em um dos nodes**:

```
sudo gluster volume start gv0

```

Confira que o seu volume existe e está replicado nos outros `nodes`:

```
sudo gluster volume info
```

![terminal-10](assets/terminal-10.png)


Nos `nodes` que não forem servers monte os dados do GlusterFS com os comandos a seguir, caso seja necessário:

```sh
sudo apt install software-properties-common
sudo add-apt-repository ppa:gluster/glusterfs-7
sudo apt install -y glusterfs-client
```

Crie a pasta para armazenar os dados:

```sh
sudo mkdir -p /data/glusterfs/brick1/gv0
sudo mount -t glusterfs vm-01:gv0 /data/glusterfs/brick1/gv0
```


---

<br>

#### Criando os primeiros PV e PVC

Como dito anteriormente, precisamos criar um **manifesto** para o _Persistent Volume_, para isso, lembre-se de preencher no campo `"server"` o IP do `master node` (caso precise saber utilize `kubectl get nodes -o wide`).

```json
{
    "kind": "PersistentVolume",
    "apiVersion": "v1",
    "metadata": {
        "name": "meu-diretorio"
    },
    "spec": {
        "capacity": {
            "storage": "1Gi"
        },
        "accessModes": ["ReadWriteMany"],
        "persistentVolumeReclaimPolicy": "Retain",
        "nfs": {
            "path": "/opt/dados",
            "server": "171.07.22.112",
            "readOnly": false
        }
    }
}
```
Ou com **GlusterFS**:
```json
{
    "kind": "PersistentVolume",
    "apiVersion": "v1",
    "metadata": {
        "name": "meu-diretorio"
    },
    "spec": {
        "capacity": {
            "storage": "1Gi"
        },
        "accessModes": ["ReadWriteMany"],
        "persistentVolumeReclaimPolicy": "Retain",
        "glusterfs": {
            "endpoints": "gluster-cluster",
            "path": "/gv0",
            "readOnly": false
        }
    }
}
```

**Agora liste o volume com `kubectl get pv` para ver se ele se encontra "Available".** 

<br>

> A declaração `"persistentVolumeReclaimPolicy"` define qual ação tomar quando o _Persistent Volume Claim_ deixa de existir, a opção escolhida _Retain_ mantém esse vínculo. É possível definir ao menos outras duas opções: _Delete_ e _Recycle_.  

<br>

O vínculo entre o _Pod_ e o _Volume_ se dá por meio de um "contrato", que também é criado por meio de um **manifesto** próprio do tipo PVC (_Persistent Volume Claim_). 

```json
{
    "kind": "PersistentVolumeClaim",
    "apiVersion": "v1",
    "metadata": {
        "name": "meu-pvc2"
    },
    "spec": {
        "accessModes": ["ReadWriteMany"],
        "resources": {
            "requests": {
                "storage": "500Mi"
            }
        }
    }
}
```

Crie e confira se o PVC existe de fato.

```
kubectl create --filename pvc-template.json
kubectl get pvc
kubectl describe pvc meu-pvc
```

Vamos repetir o passo feito no _EmptyDir_ para vincular o _Pod_ a um _volume_ só que dessa vez com o _Persistent Volume_:

1. Do último arquivo de _Deployment_ pegue a parte referente ao _Pod_ (a `"spec"` acima do `"containers"`), dentro do _container_ adicione a declaração "volumeMounts" (que é um _array_) e fora do _container_ crie o `"volumes"` (outro array) assim:

```json
        "spec": {
                "containers": [
                        {
                        "name": "nginx",
                        "image": "nginx",
                        "args": [
                            "bin/sh",
                            "-c",
                            "echo Hello \"$name\";sleep 36000"
                        ],
                        "env": [
                            {
                                "name": "name",
                                "value": "Bob"
                            }
                        ],
                        "resources": {},
                        "volumeMounts": [
                            {
                            "name": "meu-diretorio",
                            "mountPath": "/meu-diretorio"
                            }
                        ]
                    }
                ],
                "restartPolicy": "Always",
                "dnsPolicy": "ClusterFirst",
                "volumes": [
                    {
                    "name": "meu-diretorio",
                    "persistentVolumeClaim": {
                        "claimName": "meu-pvc"
                    }
                }
            ]
        }
```

2. Substitua essa parte no seu arquivo _Deployment_ e crie com o `kubectl create`

```json
{
    "kind": "Deployment",
    "apiVersion": "apps/v1",
    "metadata": {
        "name": "meuprimeirodeploy"
    },
    "spec": {
        "replicas": 1,
        "selector": {
            "matchLabels": {
                "run": "ubuntu"
            }
        },
        "template": {
            "metadata": {
                "labels": {
                    "run": "ubuntu"
                }
            },
            "spec": {
                "containers": [
                        {
                        "name": "nginx",
                        "image": "nginx",
                        "args": [
                            "bin/sh",
                            "-c",
                            "echo Hello \"$name\";sleep 36000"
                        ],
                        "env": [
                            {
                                "name": "name",
                                "value": "Bob"
                            }
                        ],
                        "resources": {},
                        "volumeMounts": [
                            {
                            "name": "meu-diretorio",
                            "mountPath": "meu-diretorio"
                            }
                        ]
                    }
                ],
                "restartPolicy": "Always",
                "dnsPolicy": "ClusterFirst",
                "volumes": [
                    {
                    "name": "meu-diretorio",
                    "persistentVolumeClaim": {
                        "claimName": "meu-pvc"
                    }
                }
            ]
        }    
    },
        "strategy": {
            "type": "RollingUpdate",
            "rollingUpdate": {
                "maxUnavailable": 5,
                "maxSurge": 2
            }
        }
    }
}
```
```
kubectl create --filename deploy-template.json
```

3. Procure o nome do _Pod_ que foi criado e acesse o shell do container `nginx`

```
kubectl get po
kubectl exec meuprimeirodeploy-6f79844954-mz4q7 -c nginx -- ls
```

**Caso uma pasta chamada _meu-diretorio_ apareça na lista é sinal de que o nosso _volume_ está funcionando**

<br>

Um PV criado em um _Deployment_ compartilha os dados entre **TODOS** os _Pods_ criados ou escalados após a criação. Vamos ver isso escalando o nosso _Deployment_ para 3 cópias:

1. Crie um arquivo no `meu-diretorio` no _Pod_ em execução com o método `exec`

```
kubectl exec meuprimeirodeploy-6f79844954-mz4q7 -c nginx -- touch /meu-diretorio/teste.json
```
2. Execute o método `scale` com 3 réplicas
```
kubectl scale deploy meuprimeirodeploy --replicas=3
```

3. Escolha um dos _Pods_ recém criados e liste o conteúdo da pasta `meu-diretorio`

```
kubectl exec meuprimeirodeploy-6f792125814-nm5z2 -c nginx -- ls /meu-diretorio
```

**Deve aparecer o arquivo _teste.json_**.

Agora experimente listar no `master node`, que possui o `nfs-server`, o conteúdo da pasta `opt/dados`. **Vai aparecer o arquivo _teste.json_ criado pelo _Pod_**.

Vamos deletar todo o _Deployment_, o PVC e o PV, para ver se o arquivo ainda permanecerá no `master node` nas pasta `opt/dados`.

```
kubectl delete deploy meuprimeirodeploy 
kubectl delete pvc meu-pvc 
kubectl delete pv meu-diretorio 
ls opt/dados
```

 **Deve continuar aparecendo o arquivo _teste.json_**.

 > Essa é a **persistência de dados**. Vale destacar que na maioria das _clouds_ ao se criar diretamento o PVC o provider dinamicamente cria o PV. 

<br>

### _StorageClass_
A criação _hard_ de volumes como demonstrado anteriormente com PV é um cenário improvável no desenvolvimento de uma aplicação distribuida de médio a grande porte. O mais comum é que o local para armazenamento seja requisitado de forma dinâmica, ou seja, quando a aplicação escalar, estender, for atualizada e gerar essa necessidade o _provider_ irá garantir esse _volume_. Para isso é necessário configurar um objeto no Kubernetes chamado _StorageClass_, que nada mais é do que um "perfil" de um _volume_. É com esse perfil que a _storage.API_ do **k8s** consegue identificar exatamente para qual _provider_ enviar a solicitação e os parâmetros armazenados.

Considerando que são **MUITAS** opções, a documentação oficial procura resumir as mais populares e explica como implentar esse objeto para maioria delas, para isso [clique aqui](https://kubernetes.io/docs/concepts/storage/storage-classes/#provisioner).

No capítulo do [_StatefulSet_](#statefulset) está implementado uma opção desse armazenamento no _local storage_ (ainda que na doc. indique que não é possível). Em produção tenha mais de um _provider_ implementado (+ de 1 _StorageClass_) e considere ter um _cluster_ próprio de armazenamento ou utilize uma nuvem pública.

<br>

## Outros controladores de _Pod_

O _Deployment_/_Replicaset_ não é o único que "contém" _Pods_ como sua menor unidade de trabalho, o **k8s** possui outros **controladores** que também se valem dos _Pods_. Nesse capítulo falaremos sobre essas outras opções e seus modos de funcionamento.

<br>

### Daemonset

O _Daemonset_ é um _Deployment_ sem o _Replicaset_, com ele não é possível controlar o número de réplicas do _Pod_, pois ele criará um _Pod_ em cada `node` do cluster que permita a criação de _Pods_. 

O seu **manifesto** é muito semelhante ao do _Deployment/Replicaset_, bastando trocar a declaração  `"kind"` para "Daemonset" e remover a declaração `"replicas"`.

```json
{
    "kind": "DaemonSet",
    "apiVersion": "apps/v1",
    "metadata": {
        "name": "meuprimeirodaemonset"
    },
    "spec": {
        "selector": {
            "matchLabels": {
                "run": "ubuntu"
            }
        },
        "template": {
            "metadata": {
                "labels": {
                    "run": "ubuntu"
                }
            },
            "spec": {
                "tolerations": [
                    {
                        "key": "node-role.kubernetes.io/master",
                        "effect": "NoSchedule"
                    }
                ],
                "containers": [
                        {
                        "name": "ubuntu",
                        "image": "ubuntu",
                        "args": [
                            "bin/sh",
                            "-c",
                            "echo Hello \"$name\";sleep 36000"
                        ],
                        "env": [
                            {
                                "name": "name",
                                "value": "Bob"
                            }
                        ],
                        "resources": {}
                        }
                    ],
                "restartPolicy": "Always",
                "dnsPolicy": "ClusterFirst"
            }    
        }
    }
}
```

Com o _DaemonSet_ também é possível fazer _Rollout/RollBack_ desde que a declaração `"updateStartegy": {"type": "RollingUpdate"}` esteja presente. Outra semelhança é o consumo dos _Volumes_, que assim como nos _Deployments_, todos os _Pods_ usam o mesmo PVC e tem acesso aos mesmos arquivos. Experimente refazer o exemplo feito no capítulo de PV e PVC, visto anteriormente.

Geralmente a escolha por um _DaemonSet_ acontece quando as aplicações precisam monitorar arquivos espalhados por todo o _cluster_, como logs ou arquivos de sistemas.


<br>

### StatefulSet

Existe a necessidade de identificar quando a aplicação foi desenvolvida de um modo _Stateless_, sem a necessidade de manter o seu estado, ou quando ela é _Stateful_, necessita manter o seu estado. Existem diversas interpretações dos conceitos dos dois (Stateless vs. Stateful), mas para simplificar podemos dizer que:

* Quando um _client_ solicita algo à aplicação e a resposta dada não depende de nenhuma interação com esse _client_ anteriormente (ex. recebimento e armazenamento de dados do usuário, sumarização de logs) dizemos que temos uma aplicação _Stateless_, por exemplo: 1 - _web servers_ sempre entregarão para todos os _clients_ os mesmos arquivos armazenados em algum lugar (status 200 ok); API REST por definição precisam ser _Stateless_ (os métodos serão os mesmos com opções de _response_ finitas);

* Quando um _client_ solicita à aplicação uma informações que ele próprio havia fornecido anterioremente dizemos que temos uma aplicação _Stateful_. Ela precisa manter o estado que o _client_ deixou para responder a solicitação seguinte, por exemplo: _Databases read & write_ dependem que o usuário deposite informações antes de solicitá-las. 

<br>

> Não serão todas as bases de dados que serão _Stateful_.

<br>

**É para o caso das aplicações _Stateful_ que o _StatefulSet_ existe!** 

O _StatefulSet_ é escalável como o _Deployment_ e não possui um _Replicaset_ como o _DaemonSet_. Sua principal característica é que o _Pod_ criado possui identidade fixa, ou seja, caso o _Pod-1_ caia, o _StatefulSet_ criará exatamente um _Pod-1_, mantendo todo seu estado. Vale mencionar que caso mais de um _Pod_ caia ele segue a ordem de criação: primeiro o _Pod-01_ levanta e libera para o _Pod-02_ começar a ser levantado e assim sucessivamente. 

Um outro ponto interessante é que cada _Pod_ definido em seu **manifesto** irá gerar um PVC, ou seja, todos eles terão _volumes_ independentes e não compartilham as edições feitas pelos outros _Pods_. 

<br>

> O problema nesse caso é que 1 PVC = 1 PV. Isso significa que quando replicar o PVC para o caso de mais de um _Pod_ _StatefulSet_ "alguém" precisa lidar com a criação dos PVs!

<br>

A solução para esse problema é chamada de **`Provisionamento Dinâmico de Volumes`**. Para esse tutorial iremos usar a opção de armazenamento local (_local storage_) disponível [nesse repositório](https://github.com/rancher/local-path-provisioner#local-path-provisioner), entretanto para ambientes de produção massivos _on prem_ utilize o [**Rook**](https://github.com/rook/rook).

Para instalação digite e confira se o _Pod_ se encontra em execução:


```
kubectl apply -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/deploy/local-path-storage.yaml
kubectl -n local-path-storage get pod

---
ubuntu@vm-01:~/rook/cluster/examples/kubernetes/ceph$ kubectl -n local-path-storage get pod
NAME                                      READY   STATUS    RESTARTS   AGE
local-path-provisioner-5696dbb894-2hrb7   1/1     Running   0          45m
```

A partir de agora não é preciso mais criar os PVs manualmente, eles serão criados automaticamente quando algum PVC for emitido. Confira o nome do `StorageClass` criado por esse _Pod_ e armazene para o passo a seguir. 

```
kubectl get sc 
```

Vamos criar o manifesto do _Statefulset_ que possui ao menos duas partes: uma de um [_Headless Service_](https://kubernetes.io/docs/concepts/services-networking/service/#headless-services) que é o responsável por controlar a identidade dos _Pods_; e a outra do _StatefulSet_ em si com sua especificação de PVC como mencionado anteriormente.  



```json
{
    "kind": "Service",
    "apiVersion": "v1",
    "metadata": {
        "name": "nginx",
        "labels": {
            "app": "nginx"
        }
    },
    "spec": {
        "ports": [
            {
                "port": 80,
                "name": "web"
            }
        ],
        "clusterIP": "None",
        "selector": {
            "app": "nginx"
        }
    }
}

{
    "kind": "StatefulSet",
    "apiVersion": "apps/v1",
    "metadata": {
        "name": "web"
    },
    "spec": {
        "selector": {
            "matchLabels": {
                "app": "nginx"
            }
        },
        "serviceName": "nginx",
        "replicas": 3,
        "template": {
            "metadata": {
                "labels": {
                    "app": "nginx"
                } 
            },
            "spec": {
                "containers": [
                        {
                        "name": "nginx",
                        "image": "nginx",
                        "ports": [
                            {
                                "containerPort": 80, 
                                "name": "web" 
                            }
                        ],
                        "volumeMounts": [
                            {
                            "name": "stateful-storage",
                            "mountPath": "/usr/share/nginx/html"
                            }
                        ]
                    }
                ]
            }
        },
        "volumeClaimTemplates": [
            {
                "metadata": {
                    "name": "stateful-storage"
                },
                "spec": {
                    "accessModes": ["ReadWriteOnce"],
                    "storageClassName": "local-path",
                    "resources": {
                        "requests": {
                            "storage": "1Gi"
                        }
                    }

                }
            }
        ]
    }
}
```

**Repare que o `"storageClassName"` deve ser o que foi obtido anteriormente.** Um outro ponto interessante de se avaliar é o nome dado ao `"volumeClaimTemplates"` ele será usado como prefixo na criação dos PVCs e será o nome do diretório montado no(s) _container(s)_ do _Pod_. 

Verifique se o _StatefulSet_ está pronto e avalie suas especificações com:

```
kubectl get sts
kubectl describe sts 
```

Agora se olharamos quantos PVs e PVCs existem repare que cada _Pod_ possui o seu próprio espaço em disco local, exatamente para manter o estado das aplicações:

```
kubectl get pv,pvc
```


### Jobs e CronJobs

Os **Jobs** não chegam a ser um _deploy_ de uma aplicação de fato, eles funcionam como uma **"esteira"** de produção, são controladores de _Pods_ como todos os outros mencionados anteriormente mas sua principal característica é garantir que o _Pod_ por ele iniciado seja considerado _finalizado_ em algum momento.

<br>

> A documentação oficial trata os **Jobs** como **"tasks"** e de fato o são, porém deve-se ter cuidado ao entender dessa forma pois um **CustomResourceDefinition** que vem ganhando força recentemente, o [Tekton](https://tekton.dev/), utiliza essa mesma nomenclatura para um dos principais objetos de trabalho deles.

<br>

A composição de **Job** segue a estrutura básica mencionada anteriormente para outros controlares:

1. Preencha o _rascunho_ usado na maioria dos objetos do **k8s**
```
{
    "kind": "Job",
    "apiVersion": "batch/v1",
    "metadata": {
        "name": "pi"
    },
    "spec": {
        "template": {}
    }
}
```

2. Pegue o as especificações (**spec**) do container que deseja (contendo a chave `"command"`, que veremos em detalhes na próxima seção) e insera na chave `"template"`.

```
{
  "kind": "Job",
  "apiVersion": "batch/v1",
  "metadata": {
      "name": "pi-node"
  },
  "spec": {
     "template": {
         "spec": {
             "containers": [
                  {
                      "name": "pi-node",
                      "image": "mhart/alpine-node:base-0.10",
                      "command": ["sh", "-c", "/bin/echo 'console.log(\"I, Nodejs, know pi: \" + Math.PI)' | /usr/bin/env node"]
                  }
        ],
             "restartPolicy": "Never"     
      }
    } 
  }
}
```

3. Crie o _Job_ e confira se a tarefa foi completada.

```
kubectl create -f job.json
kubectl get jobs pi-node

---
NAME      COMPLETIONS   DURATION   AGE
pi-node   1/1           6s         10s
```

4. Descreva o _Job_ para obter o nome do _container_ e confira os logs para ver se o NodeJS imprime o valor de Pi.

```
kubectl describe job/pi-node
kubectl logs po/pi-node-dhlds

---
I, Nodejs, know pi: 3.141592653589793
```

<br>

>_Containers_ são efêmeros. Eles existem até o momento de execução da sua tarefa, depois de concluída eles deixam de existir. Uma tarefa é caracterizada pelo _command_ ou _entrypoint_ definido na _build_ imagem ou no momento de execução. Caso deixem de existir sem erros, então o _Job_ é considerado completado. 

<br>

Até que o _Job_ possa ser considerado `_completed_` ele irá reiniciar os _Pods_ quando algum deles apresentar erro, o que em consequência pode gerar um _loop_ de erro com um "fim" longo, podendo durar até 6 minutos consumindo recursos do _cluster_. Para que se evite esse cenário é boa prática determinar o número máximo de tentativas de um _Job_ com a declaração `".spec.backoffLimit"`.

```json
{
  "kind": "Job",
  "apiVersion": "batch/v1",
  "metadata": {
      "name": "pi-node"
  },
  "spec": {
     "template": {
         "spec": {
             "backoffLimit": 5,
             "containers": [
                  {
                      "name": "pi-node",
                      "image": "mhart/alpine-node:base-0.10",
                      "command": ["sh", "-c", "/bin/echo 'console.log(\"I, Nodejs, know pi: \" + Math.PI)' | /usr/bin/env node"]
                  }
        ],
             "restartPolicy": "Never"     
      }
    }
  }
}
```

É possível rodar o mesmo _Job_ mais de uma vez especificando o número de `"spec.completions"` que se deseja, nesse caso um  _Pod_ de cada vez será iniciado até que o número definido seja alcançado. Vamos ver na prática, abaixo está o **manifesto** do mesmo _Job_ feito anteriormente só que agora com 2 _Pods_ emitindo a mensagem:

```json
{
  "kind": "Job",
  "apiVersion": "batch/v1",
  "metadata": {
      "name": "pi-node"
  },
  "spec": {
     "backoffLimit": 5,
     "completions": 2,
     "template": {
         "spec": {
             "containers": [
                  {
                      "name": "pi-node",
                      "image": "mhart/alpine-node:base-0.10",
                      "command": ["sh", "-c", "/bin/echo 'console.log(\"I, Nodejs, know pi: \" + Math.PI)' | /usr/bin/env node"]
                  }
        ],
             "restartPolicy": "Never"     
      }
    }
  }
}
```

Verifique se os _Pods_ foram criandos corretamente com:
```
kubectl get events
```

Se houver a necessidade é possível definir _Pods_ que rodem em parelelo com `".spec.parallelism"`, assim o _Job_ irá iniciar o número determinado ao mesmo tempo, contudo, caso não seja especificado o número de `".spec.completions"`, basta que apenas um desses _Pod_ seja concluído sem erro que o _Job_ será considerado um sucesso, todos os outros _Pods_ criados serão concluídos mas seus _status_ não importarão mais. 

```json
{
  "kind": "Job",
  "apiVersion": "batch/v1",
  "metadata": {
      "name": "pi-node"
  },
  "spec": {
     "backoffLimit": 5,
     "parallelism": 2,
     "template": {
         "spec": {
             "containers": [
                  {
                      "name": "pi-node",
                      "image": "mhart/alpine-node:base-0.10",
                      "command": ["sh", "-c", "/bin/echo 'console.log(\"I, Nodejs, know pi: \" + Math.PI)' | /usr/bin/env node"]
                  }
        ],
             "restartPolicy": "Never"     
      }
    }
  }
}
```

Por fim o último padrão é a união do `".spec.parallelism"` com o  `".spec.completions"`, nesse cenário serão criados _Pods_ em paralelo até o limite que restar para o número especificado no _completions_ ser atingido. 

```json
{
  "kind": "Job",
  "apiVersion": "batch/v1",
  "metadata": {
      "name": "pi-node"
  },
  "spec": {
     "backoffLimit": 5,
     "parallelism": 2,
     "completions": 2,
     "template": {
         "spec": {
             "containers": [
                  {
                      "name": "pi-node",
                      "image": "mhart/alpine-node:base-0.10",
                      "command": ["sh", "-c", "/bin/echo 'console.log(\"I, Nodejs, know pi: \" + Math.PI)' | /usr/bin/env node"]
                  }
        ],
             "restartPolicy": "Never"     
      }
    }
  }
}
```

<br>

**Jobs considerados _completed_ não irão excluir os _Pods_ criados ainda que eles não executem mais o comando definido**. Para que a deleção automática do _Job_ ocorra é preciso informar o `".spec.ttlSecondsAfterFinished"`, nesse contexto ttl significa _time to live_, o que basicamente define quanto tempo (em segundos) após o _Job_ ser considerado _completed_ que os logs e recursos ficarão disponíveis para consulta. 

<br>

```json
{
  "kind": "Job",
  "apiVersion": "batch/v1",
  "metadata": {
      "name": "pi-node"
  },
  "spec": {
     "backoffLimit": 5,
     "parallelism": 2,
     "completions": 2,
     "ttlSecondsAfterFinished": 200,
     "template": {
         "spec": {
             "containers": [
                  {
                      "name": "pi-node",
                      "image": "mhart/alpine-node:base-0.10",
                      "command": ["sh", "-c", "/bin/echo 'console.log(\"I, Nodejs, know pi: \" + Math.PI)' | /usr/bin/env node"]
                  }
        ],
             "restartPolicy": "Never"     
      }
    }
  }
}
```

---

<br>

As **CronJobs** são controladores dos _Jobs_ e possuem basicamente a função de executá-los em intervalos de tempo determinados, segundo uma [CronTab](https://crontab.guru/). A sua implementação é bem simples, basta informar na declaração `".spec.schedule"` a CronTab e no `".spec.jobTemplate"` toda a especificação do _Job_ como demonstrado anteriormente. 

```
{
  "kind": "CronJob",
  "apiVersion": "batch/v1",
  "metadata": {
      "name": "pi-node"
  },
  "spec": {
       "schedule": "* /1 * * * *",
       "jobTemplate": {
            "spec": {
                 "backoffLimit": 5,
                 "parallelism": 2,
                 "completions": 2,
                 "ttlSecondsAfterFinished": 200,
                 "template": {
                     "spec": {
                         "containers": [
                              {
                                  "name": "pi-node",
                                  "image": "mhart/alpine-node:base-0.10",
                                  "command": ["sh", "-c", "/bin/echo 'console.log(\"I, Nodejs, know pi: \" + Math.PI)' | /usr/bin/env node"]
                              }
                    ],
                         "restartPolicy": "Never"     
                  }
                }
            }
       }
  }
}
```

<br>

>Existem duas outras especificações referente a CronJob que lidam com o atraso de inicialização do _Pod_ e podem ser consultadas na documentação oficial.

<br>


## Gerenciando os Pods

O controle que o **k8s** oferece na orquestração de _containers_ é basicamente sustentado pelo conhecimento que o usuário possui das possibilidades de customização de _Pods_. Nessa seção iremos tratar de algumas dessas opções e seus casos de aplicação. 

### Limitando recursos no Pod
Um _Pod_ que contenha _containers_ com recursos limitados pode ser criado assim: 

```
kubectl run nginx --dry-run=client -o json --image=nginx --limits 'cpu=0.2,memory=512Mi'> pod-template.json

cat pod-template.json
```

```json
{
    "kind": "Pod",
    "apiVersion": "v1",
    "metadata": {
        "name": "nginx",
        "creationTimestamp": null,
        "labels": {
            "run": "nginx"
        }
    },
    "spec": {
        "containers": [
            {
                "name": "nginx",
                "image": "nginx",
                "resources": {
                    "limits": {
                        "cpu": "200m",
                        "memory": "512Mi"
                    }
                }
            }
        ],
        "restartPolicy": "Always",
        "dnsPolicy": "ClusterFirst"
    },
    "status": {}
}
```

Dessa vez a declaração `"resources"` possui um valor que é um outro objeto declarado como `"limits"`. Dentro dele podemos ter até três chaves: _cpu_, _memory_ e _ephemeral storage_ (mais incomum, então consulte a [documentação](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#configurations-for-local-ephemeral-storage)).

Vale lembrar que os `"limits"` determinam o **máximo que pode ser consumido para aquele _container_**. 

Existe a opção de **"reservar"** um recurso mínimo para aquele _container_ funcionar com a declaração `"requests"`.

```
kubectl run nginx --dry-run=client -o json --image=nginx --requests 'cpu=0.2,memory=512Mi'> pod-template.json

cat pod-template.json

```

```json
{
    "kind": "Pod",
    "apiVersion": "v1",
    "metadata": {
        "name": "nginx",
        "creationTimestamp": null,
        "labels": {
            "run": "nginx"
        }
    },
    "spec": {
        "containers": [
            {
                "name": "nginx",
                "image": "nginx",
                "resources": {
                    "requests": {
                        "cpu": "200m",
                        "memory": "512Mi"
                    }
                }
            }
        ],
        "restartPolicy": "Always",
        "dnsPolicy": "ClusterFirst"
    },
    "status": {}
}
```

Desse jeito certamente o _container_ tem 20% de um processador e 512MB (aproximadamente) da memória, antes, com o `"limits"` ele **poderia** chegar a usar isso. 

Eventualmente, limitar os recursos utilizados por cada _container_ faz com que você limite o _Pod_ como um todo. 

A seguir temos o **manifesto** de um _Pod_ que reserva 512MB (256MB para o **nginx** e 256MB para o **ubuntu**), mas pode chegar a 1GB (512MB para cada) e reserva 50% de uma cpu (250m para cada), podendo chegar a 1 cpu completa.

```json
{
  "kind": "Pod",
  "apiVersion": "v1",
  "metadata": {
    "name": "nginx-ubuntu",
    "labels": {
      "app": "webapp"
    }
  },
  "spec": {
    "containers": [
      {
        "name": "nginx",
        "image": "nginx",
        "resources": {
            "requests": {
                "cpu": "250m",
                "memory": "256Mi"
            },
            "limits": {
                "cpu": "500m",
                "memory": "512Mi"
            }
        }
      },
      {
        "name": "ubuntu",
        "image": "ubuntu",
        "command": ["/bin/sh", "-c"],
        "args": ["echo Hello world; sleep 36000"],
        "resources": {
            "requests": {
                "cpu": "250m",
                "memory": "256Mi"
            },
            "limits": {
                "cpu": "500m",
                "memory": "512Mi"
            }
        }
      }
    ]
  }
}
```

<br>

> Permitir que as aplicações possam consumir mais limite de _hardware_ do que foi determinado no momento de seu _deploy_ é uma técnica conhecida como **escalonamento vertical**

<br>

Vale mencionar que é o `Scheduler` o responsável por identificar o `node` que possui os recursos disponíveis para implementação do _Pod_, avaliando pelo `"limits"` quando estiver presente. Podemos ver isso ao implementar o _Pod_ e checar os eventos em seguida:

```
kubectl create --filename pod-template.json
kubectl get events
```

```
LAST SEEN   TYPE      REASON        OBJECT             MESSAGE
56m         Normal    Scheduled     pod/nginx-ubuntu   Successfully assigned default/nginx-ubuntu to vm-03
```

Caso o recurso não esteja disponível em nenhum `node` o `Scheduler` **não implementa o _Pod_**. 

Vamos subir os `"requests"` para mais processadores do que os `nodes` possuem:

```json
{
  "kind": "Pod",
  "apiVersion": "v1",
  "metadata": {
    "name": "nginx-ubuntu",
    "labels": {
      "app": "webapp"
    }
  },
  "spec": {
    "containers": [
      {
        "name": "nginx",
        "image": "nginx",
        "resources": {
            "requests": {
                "cpu": "3000m",
                "memory": "256Mi"
            },
            "limits": {
                "cpu": "4000m",
                "memory": "512Mi"
            }
        }
      },
      {
        "name": "ubuntu",
        "image": "ubuntu",
        "command": ["/bin/sh", "-c"],
        "args": ["echo Hello world; sleep 36000"],
        "resources": {
            "requests": {
                "cpu": "3000m",
                "memory": "256Mi"
            },
            "limits": {
                "cpu": "4000m",
                "memory": "512Mi"
            }
        }
      }
    ]
  }
}
```
Agora cheque o _status_ do _Pod_ e poderá ver que ele está em _Pending_, ou seja, esperando até que o _hardware_ solicitado esteja disponível.

```
kubectl get po
```

![terminal-1](assets/terminal-1.png)

Confira os eventos e verá o motivo disso:

![terminal-2](assets/terminal-2.png)

Repare que nesse último **manifesto** o _container_ da imagem do **ubuntu** contém duas novas declarações: **command** e **args**. 

<br>

### Comandos, Argumentos e Variáveis
Ao iniciar o _container_ é possível definir as ações que ele irá executar com as chaves `"command"` e `"args"`. Vamos iniciar aquela imagem do **ubuntu** com um "Hello World" e um tempo de espera de 36000 segundos (para o k8s não reclamar que o processo se encerra antes do [_check readiness_](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/)):

```
kubectl run ubuntu --dry-run=client -o json --image=nginx -- bin/sh -c "echo 'Hello World';sleep 36000" > pod-template.json
```

```json
{
    "kind": "Pod",
    "apiVersion": "v1",
    "metadata": {
        "name": "ubuntu",
        "creationTimestamp": null,
        "labels": {
            "run": "ubuntu"
        }
    },
    "spec": {
        "containers": [
            {
                "name": "ubuntu",
                "image": "nginx",
                "args": [
                    "bin/sh",
                    "-c",
                    "echo 'Hello World';sleep 36000"
                ],
                "resources": {}
            }
        ],
        "restartPolicy": "Always",
        "dnsPolicy": "ClusterFirst"
    },
    "status": {}
}

```
Execute o pod e confira os logs:

```
kubectl create --filename pod-template.json

kubetcl logs ubuntu

```
Deve aparecer o _"Hello World"_.

Repare que caso nada seja especificado nos **"--"** o `kubectl` irá considerar que tudo o que vier é **argumento**. Caso deseje que seja entendido como **comando** basta trocar o **"--"** para **"--command"**.

```
kubectl run ubuntu --dry-run=client -o json --image=nginx --command bin/sh -- -c "echo 'Hello World';sleep 36000" > pod-template.json
```

```json
{
    "kind": "Pod",
    "apiVersion": "v1",
    "metadata": {
        "name": "ubuntu",
        "creationTimestamp": null,
        "labels": {
            "run": "ubuntu"
        }
    },
    "spec": {
        "containers": [
            {
                "name": "ubuntu",
                "image": "nginx",
                "command": [
                    "bin/sh",
                    "-c",
                    "echo 'Hello World';sleep 36000"
                ],
                "resources": {}
            }
        ],
        "restartPolicy": "Always",
        "dnsPolicy": "ClusterFirst"
    },
    "status": {}
}
```

Na prática os dois irão executar o container desse _Pod_ da mesma forma. Vamos experimentar inserindo **variáveis de ambiente**.

```
kubectl run ubuntu --dry-run=client -o json --image=nginx --env="name=Bob" -- bin/sh -c "echo Hello $name;sleep 36000" > pod-template.json
```
```json
{
    "kind": "Pod",
    "apiVersion": "v1",
    "metadata": {
        "name": "ubuntu",
        "creationTimestamp": null,
        "labels": {
            "run": "ubuntu"
        }
    },
    "spec": {
        "containers": [
            {
                "name": "ubuntu",
                "image": "nginx",
                "args": [
                    "bin/sh",
                    "-c",
                    "echo Hello \"$name\";sleep 36000"
                ],
                "env": [
                    {
                        "name": "name",
                        "value": "Bob"
                    }
                ],
                "resources": {}
            }
        ],
        "restartPolicy": "Always",
        "dnsPolicy": "ClusterFirst"
    },
    "status": {}
}
```
Execute o _Pod_ e confira os logs:

```
kubectl create --filename pod-template.json

kubetcl logs ubuntu
```

Deve aparecer o _"Hello Bob"_.


> Vale mencionar que existe o objeto `ConfigMap` que é feito para armazenar variáveis de ambiente de uma maneira global e os `Secrets` que definem as que são sensíveis à exposição.

<br>

### _Init containers_

O objetivo do _InitContainer_ é executar comandos antes que os containers definidos pelo _Pod controller_ possam ser iniciados. A ideia é semelhante a um _Job_ executado antes que o _Pod_ principal suba, a documentação oficial sugere que se use esse recurso para: 

* criar um _fluxo de deployment_, ou seja, estabelecer ordem no tempo de início de cada um; 
* para fazer o registro desse deploy em algum tipo de base de dados ou sistema de mensageria; e 
* para popular um _volume_ com algum download ou git clone. 

Abaixo temos um exemplo de aplicação, ele instala o Vue CLI (biblioteca Javacript para front-end) em um container `node`, carrega uma _Single Page Application default_ e exporta para pasta `dist`. Após esse processo inicia-se um container `nginx` que copia o conteúdo da pasta `dist` para a pasta de arquivos estáticos onde irá servir a aplicação. 

<br>

>Lembre-se que esse cenário é apenas para fins didáticos, existem formas mais eficientes de fazer o build abaixo.
<br>

```json
{
  "kind": "Pod",
  "apiVersion": "v1",
  "metadata": {
      "name": "app-vue",
      "labels": {
          "app": "vue"
      }
  },
  "spec": {
      "containers": [
          {
              "name": "nginx",
              "image": "nginx",
              "ports": [
                  {
                      "containerPort": 80
                  }
              ],
              "command": [
                  "sh",
                  "-c",
                  "cp -rf emptydir/hello-world/dist/. usr/share/nginx/html && /usr/sbin/nginx -g 'daemon off;'"
              ],
              "volumeMounts": [
                          {
                          "name": "emptydir",
                          "mountPath": "/emptydir"
                          }
              ]
          }
      ],
      "initContainers": [
            {
              "name": "vue",
              "image": "node",
              "command": [
                "sh",
                "-c",
                "cd emptydir && npm install -g @vue/cli && vue create hello-world -d && cd hello-world && npm run build"
              ],
              "volumeMounts": [
                          {
                          "name": "emptydir",
                          "mountPath": "/emptydir"
                          }
                  ]
            }
          ],
      "volumes": [
                  {
                  "name": "emptydir",
                  "emptyDir": {}
                  }
          ]
    }    
}
```

```
kubectl get po

---
NAME            READY   STATUS      RESTARTS   AGE
app-vue        0/1     Init:0/1    0          20m
```


Repare que o _Pod_ ficará com o status de "Init: 0/1" até que o InitContainer tenha feito a build da aplicação. Quando o status _"Running"_ aparecer faça um `port-forward` para o localhost:80 (o node master no caso).

```
kubectl port-forward po/app-vue 80:80
```

E verifique se a página inicial do VueJS aparece no browser no `localhost:80`. 



### _Pods_ prioritários

Um recurso um pouco complexo no gerenciamento de _Pods_ é a prioridade de deploy. Assim como é possível estabelecer uma classe para os _volumes_ com o objeto _StorageClass_ também é possível estabelecer uma classe para os _Pods_ com o objeto _PriorityClass_, nesse caso é essa classe que irá definir qual _Pod_ será criado antes do outro.

Um objeto _PriorityClass_ é definido assim:

```json
{
    "kind": "PriorityClass",
    "apiVersion": "scheduling.k8s.io/v1",
    "metadata": {
        "name": "grupo-1-classe-d"
    },
    "value": 3,
    "preemptionPolicy": "Never",
    "globalDefault": false,
    "description": "Classe dentro do grupo-1 de menor importância de deployment."
}
```

Vamos definir outra classe de um valor maior:

```json
{
    "kind": "PriorityClass",
    "apiVersion": "scheduling.k8s.io/v1",
    "metadata": {
        "name": "grupo-1-classe-c"
    },
    "value": 5,
    "preemptionPolicy": "Never",
    "globalDefault": false,
    "description": "Classe dentro do grupo-1 de segunda menor importância de deployment."
}
```

Verifique quais são as classes disponíveis:

```
kubectl get pc

---
NAME                      VALUE        GLOBAL-DEFAULT   AGE
grupo-1-classe-c          5            false            60s
grupo-1-classe-d          3            false            60s
system-cluster-critical   2000000000   false            2d11h
system-node-critical      2000001000   false            2d11h
```

Repare que a API do **k8s** já utiliza duas classes para ordenamento de seus deploys, sendo a maior delas a que é crítica ao node. As nossas duas novas classes recém criadas também já estão disponíveis para uso. 

Agora, em teoria, caso dois _Deployments_ com vários _Pods_ sejam solicitados ao mesmo tempo, a prioridade de agendamento pelo _Scheduler_ deve ser com o _Pod_ de maior valor de classe. Vamos testar isso inserindo dois _Pods_ em um mesmo arquivo, para que possam ser enviados ao mesmo tempo à API de **k8s**, cada um com uma classe. 

```json
{
    "kind":"Pod",
    "apiVersion": "v1",
    "metadata": {
        "name": "nginx-1"
    },
    "spec": {
        "containers": [
            {
                "name": "nginx-1",
                "image": "nginx"
            }
        ],
        "priorityClassName": "grupo-1-classe-d"
    }
}

{
    "kind":"Pod",
    "apiVersion": "v1",
    "metadata": {
        "name": "nginx-2"
    },
    "spec": {
        "containers": [
            {
                "name": "nginx-2",
                "image": "nginx"
            }
        ],
        "priorityClassName": "grupo-1-classe-c"
    }
}
```

Vamos criar e conferir os eventos que se seguem:
```
kubectl create -f pod.json
kubectl get deploy -w

---
NAME      READY   UP-TO-DATE   AVAILABLE   AGE
nginx-1   2/50    50           2           2m14s
nginx-2   1/50    50           1           2m14s
nginx-1   3/50    50           3           2m22s
nginx-1   4/50    50           4           2m23s
nginx-2   2/50    50           2           2m40s
nginx-2   3/50    50           3           2m41s
nginx-2   4/50    50           4           2m42s
nginx-2   5/50    50           5           2m54s
nginx-2   6/50    50           6           2m56s
nginx-2   7/50    50           7           2m58s
nginx-2   8/50    50           8           2m59s
nginx-1   5/50    50           5           2m59s
nginx-2   9/50    50           9           3m
nginx-2   10/50   50           10          3m1s
nginx-2   11/50   50           11          3m8s
...
nginx-2   49/50   50           49          5m8s
nginx-2   50/50   50           50          5m10s
nginx-1   49/50   50           49          5m12s
nginx-1   50/50   50           50          5m14s
```

**Repare que a maior parte do tempo o _Scheduler_ tenta manter o número do segundo deploy acima do primeiro e ao final ele encerra antes que o primeiro**. Isso só acontece porque o tempo todo existe uma avaliação do valor da classe do _Pod_. 

Quando a opção `"preemptionPolicy"` está em `"PreemptLowerPriority"` caso não existam recursos no _cluster_ o _Scheduler_ retira um _Pod_ de menor prioridade para alocar o recurso para aquele de maior prioridade. Esse cenário só acontece quando existe um _cluster_ saturado de _Pods_. 
